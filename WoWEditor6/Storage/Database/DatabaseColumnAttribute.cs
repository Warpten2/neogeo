﻿using System;

namespace WoWEditor6.Storage.Database
{
    class DatabaseColumnAttribute : Attribute
    {
        public string ColumnName { get; private set; }
        public string[] MultiColumns { get; private set; }

        public bool IsMultiColumn { get; private set; }

        public DatabaseColumnAttribute(string columnName)
        {
            IsMultiColumn = false;
            ColumnName = columnName;
        }

        public DatabaseColumnAttribute(params string[] columnNames)
        {
            IsMultiColumn = true;
            MultiColumns = columnNames;
        }
    }
}
