﻿using System;
using System.Text;

namespace WoWEditor6.Storage.Database
{
    class RemoteTableColumnAttribute : Attribute
    {
        public string[] SelectColumns { get; set; }

        public string[] RemoteColumnCondition { get; set; }
        public string[] TargetColumnCondition { get; set; }
        public string RemoteTable { get; set; }
        public string TargetTable { get; set; }
        public string JoinType { get; set; }

        public RemoteTableColumnAttribute(string[] remoteColumn, string[] targetColumn, string remoteTable, string targetTable)
        {
            RemoteColumnCondition = remoteColumn;
            TargetColumnCondition = targetColumn;
            RemoteTable = remoteTable;
            TargetTable = targetTable;
        }

        public string GetJoin()
        {
            var str = new StringBuilder();
            str.Append($" {JoinType} JOIN `{RemoteTable}`");

            var loopMax = Math.Min(RemoteColumnCondition.Length, TargetColumnCondition.Length);

            var joinFilters = new string[loopMax];
            for (var i = 0; i < loopMax; ++i)
                joinFilters[i] = $"ON `{RemoteTable}`.`{RemoteColumnCondition[i]} = `{TargetTable}`.`{TargetColumnCondition[i]}`";

            str.Append(string.Join(", ", joinFilters));
            return str.Append(" ").ToString();
        }

        public string GetSelects()
        {
            var str = new string[SelectColumns.Length];
            for (var i = 0; i < str.Length; ++i)
                str[i] = $"`{RemoteTable}`.`{SelectColumns[i]}`";
            return string.Join(", ", str);
        }
    }
}
