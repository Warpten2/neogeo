﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WoWEditor6.Storage.Database
{
    class DatabaseLoader<T> where T : class, new()
    {
        public static string GetLoadQuery(string tableName)
        {
            var sb = new StringBuilder();
            sb.Append("SELECT ");

            var columns = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(c => c.GetGetMethod() != null &&
                    (c.GetCustomAttribute<DatabaseColumnAttribute>() != null ||
                     c.GetCustomAttribute<RemoteTableColumnAttribute>() != null));
            var fIndex = 0;
            var joins = new List<string>();
            foreach (var column in columns)
            {
                if (column.GetCustomAttribute<DatabaseColumnAttribute>() == null)
                    if (column.GetCustomAttribute<RemoteTableColumnAttribute>() == null)
                        continue;

                var databaseColumnAttr = column.GetCustomAttribute<DatabaseColumnAttribute>();
                var remoteColumnAttr = column.GetCustomAttribute<RemoteTableColumnAttribute>();
                if (databaseColumnAttr != null)
                {
                    ++fIndex;

                    sb.Append(" `");
                    if (databaseColumnAttr.IsMultiColumn)
                    {
                        var colIndex = 0;
                        foreach (var col in databaseColumnAttr.MultiColumns)
                        {
                            ++colIndex;
                            sb.Append(col);
                            if (colIndex != databaseColumnAttr.MultiColumns.Length)
                                sb.Append("`, `");
                        }
                    }
                    else
                        sb.Append(databaseColumnAttr.ColumnName);

                    sb.Append("` ");
                    if (fIndex != columns.Count())
                        sb.Append(", ");
                }
                else if (remoteColumnAttr != null)
                {
                    ++fIndex;

                    sb.Append(remoteColumnAttr.GetSelects());
                    joins.Add(remoteColumnAttr.GetJoin());
                }
            }

            sb.Append(" FROM ");
            sb.Append(tableName);
            if (joins.Count > 0)
                sb.Append(string.Concat(", ", joins.ToArray()));
            return sb.ToString();
        }

        public void Load(DataRow dataRow)
        {
            var columns = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var column in columns)
            {
                if (column.GetSetMethod() == null)
                    continue;

                var attr = column.GetCustomAttribute<DatabaseColumnAttribute>();
                if (attr == null)
                    continue;

                if (attr.IsMultiColumn)
                {
                    var val = Array.CreateInstance(column.PropertyType.GetElementType(), attr.MultiColumns.Length);

                    for (var i = 0; i < attr.MultiColumns.Length; ++i)
                    {
                        try
                        {
                            var objVal = dataRow[attr.MultiColumns[i]];
                            if (column.PropertyType.GetElementType().IsEnum)
                                objVal = Enum.ToObject(column.PropertyType.GetElementType(), objVal);

                            val.SetValue(objVal, i);
                        }
                        catch (Exception ex)
                        {
                            Log.Error($"An error occured while reading column `{attr.MultiColumns[i]}`:");
                            Log.Error(ex.Message);
                        }
                    }

                    column.SetValue(this, val);
                }
                else
                    try
                    {
                        var objVal = dataRow[attr.ColumnName];
                        if (column.PropertyType.IsEnum)
                            objVal = Enum.ToObject(column.PropertyType, objVal);

                        column.SetValue(this, objVal);
                    }
                    catch (Exception ex)
                    {
                        Log.Error($"An error occured while reading column `{attr.ColumnName}`:");
                        Log.Error(ex.Message);
                    }
            }
        }
    }
}
