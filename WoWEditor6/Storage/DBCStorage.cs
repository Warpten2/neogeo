﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WoWEditor6.IO.Files;
using WoWEditor6.IO.Files.Sky;
using WoWEditor6.IO.Files.Structures;

namespace WoWEditor6.Storage
{
    static class DbcStorage
    {
        public static DBFile<MapEntry> Map { get; private set; }
        public static DBFile<LoadingScreenEntry> LoadingScreens { get; private set; }
        public static DBFile<LightEntry> Light { get; private set; }

        public static DBFile<LightDataEntry> LightData { get; private set; }
        public static DBFile<LightParamsEntry> LightParams { get; private set; }
        public static DBFile<ZoneLightEntry> ZoneLight { get; private set; }
        public static DBFile<ZoneLightPointEntry> ZoneLightPoint { get; private set; }
        public static DBFile<CharSectionsEntry> CharSections { get; private set; }
        public static DBFile<CreatureDisplayInfoEntry> CreatureDisplayInfo { get; private set; }
        public static DBFile<CreatureDisplayInfoExtraEntry> CreatureDisplayInfoExtra { get; private set; }
        public static DBFile<CreatureModelDataEntry> CreatureModelData { get; private set; }
        public static DBFile<GameObjectDisplayInfoEntry> GameObjectDisplayInfo { get; private set; }
        public static DBFile<FileDataEntry> FileData { get; private set; }
        public static DBFile<ItemDisplayInfoEntry> ItemDisplayInfo { get; private set; }
        public static DBFile<GroundEffectTextureEntry> GroundEffectTexture { get; private set; }
        public static DBFile<GroundEffectDoodadEntry> GroundEffectDoodad { get; private set; }
        public static DBFile<TextureFileDataEntry> TextureFileData { get; private set; }
        public static DBFile<CharComponentTextureSectionEntry> CharComponentTextureSections { get; private set; }
        public static DBFile<CharComponentTextureLayoutEntry> CharComponentTextureLayouts { get; private set; }
        public static DBFile<AreaTriggerEntry> AreaTrigger { get; private set; }

        public static void Initialize()
        {
            Map                          = Open<MapEntry>("Map");
            LoadingScreens               = Open<LoadingScreenEntry>("LoadingScreens");
            Light                        = Open<LightEntry>("Light");
            LightData                    = Open<LightDataEntry>("LightData");
            LightParams                  = Open<LightParamsEntry>("LightParams");
            ZoneLight                    = Open<ZoneLightEntry>("ZoneLight");
            ZoneLightPoint               = Open<ZoneLightPointEntry>("ZoneLightPoint");
            CharSections                 = Open<CharSectionsEntry>("CharSections");
            CreatureDisplayInfo          = Open<CreatureDisplayInfoEntry>("CreatureDisplayInfo");
            CreatureDisplayInfoExtra     = Open<CreatureDisplayInfoExtraEntry>("CreatureDisplayInfoExtra");
            GameObjectDisplayInfo        = Open<GameObjectDisplayInfoEntry>("GameObjectDisplayInfo");
            CreatureModelData            = Open<CreatureModelDataEntry>("CreatureModelData");
            FileData                     = Open<FileDataEntry>("FileData");
            CharSections                 = Open<CharSectionsEntry>("CharSections");
            ItemDisplayInfo              = Open<ItemDisplayInfoEntry>("ItemDisplayInfo");
            GroundEffectDoodad           = Open<GroundEffectDoodadEntry>("GroundEffectDoodad");
            GroundEffectTexture          = Open<GroundEffectTextureEntry>("GroundEffectTexture");
            TextureFileData              = Open<TextureFileDataEntry>("TextureFileData");
            CharComponentTextureSections = Open<CharComponentTextureSectionEntry>("CharComponentTextureSections");
            CharComponentTextureLayouts  = Open<CharComponentTextureLayoutEntry>("CharComponentTextureLayouts");
            AreaTrigger                  = Open<AreaTriggerEntry>("AreaTrigger");

            SkyManager.Instance.Initialize();
        }

        private static Dictionary<Type, PropertyInfo> mTypeCache = new Dictionary<Type, PropertyInfo>();

        public static DBFile<T> Open<T>(string fileName) where T : class, new()
        {
            foreach (var field in typeof(DbcStorage).GetProperties(BindingFlags.Public | BindingFlags.Static))
                if (field.Name == fileName)
                    mTypeCache[typeof(T)] = field;

            return new DBFile<T>(fileName);
        }

        public static DBFile<T> OfType<T>() where T : class, new()
        {
            if (!mTypeCache.ContainsKey(typeof(T)))
                return null;

            return (DBFile<T>)mTypeCache[typeof(T)].GetValue(null);
        }
    }
}
