﻿using SharpDX;
using System;
using System.Linq;
using System.Collections.Generic;

namespace WoWEditor6.Utils
{
    static class MathExtensions
    {
        public static BoundingBox Transform(this BoundingBox box, ref Matrix matrix)
        {
            var corners = new Vector3[8];
            box.GetCorners(corners);
            var newMin = new Vector3(float.MaxValue);
            var newMax = new Vector3(float.MinValue);

            for(var i = 0; i < corners.Length; ++i)
            {
                Vector3 v;
                Vector3.TransformCoordinate(ref corners[i], ref matrix, out v);
                TakeMin(ref newMin, ref v);
                TakeMax(ref newMax, ref v);
            }

            return new BoundingBox(newMin, newMax);
        }

        public static float ToDegree(this float radians)
        {
            return MathUtil.RadiansToDegrees(radians);
        }

        public static float ToRadians(this float degrees)
        {
            return MathUtil.DegreesToRadians(degrees);
        }

        public static void TakeMin(ref Vector3 v , ref Vector3 other)
        {
            if (v.X > other.X)
                v.X = other.X;
            if (v.Y > other.Y)
                v.Y = other.Y;
            if (v.Z > other.Z)
                v.Z = other.Z;
        }

        public static void TakeMax(ref Vector3 v, ref Vector3 other)
        {
            if (v.X < other.X)
                v.X = other.X;
            if (v.Y < other.Y)
                v.Y = other.Y;
            if (v.Z < other.Z)
                v.Z = other.Z;
        }

        public static Vector2 ToVector2(this Vector3 position)
        {
            return new Vector2(position.X, position.Y);
        }

        public static Vector3 ToWorldCoordinates(this Vector3 position)
        {
            return new Vector3(32.0f * Metrics.TileSize - position.Y, position.X + 32.0f * Metrics.TileSize, position.Z);
        }

        public static Vector3 ToDatabaseCoordinates(this Vector3 position)
        {
            return new Vector3(position.Y - 32.0f * Metrics.TileSize, 32.0f * Metrics.TileSize - position.X, position.Z);
        }

        public static T RandomElement<T>(this IEnumerable<T> enumerable, Func<T, bool> filter)
        {
            return enumerable.Where(filter).RandomElementUsing(new Random());
        }

        public static T RandomElement<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.RandomElementUsing(new Random());
        }

        public static T RandomElementUsing<T>(this IEnumerable<T> enumerable, Random rand)
        {
            int index = rand.Next(0, enumerable.Count());
            return enumerable.ElementAt(index);
        }
    }
}
