﻿using WoWEditor6.Storage.Database;

namespace WoWEditor6.Scene.Entities.GameObject
{
    class GameObjectTemplate : DatabaseLoader<GameObjectTemplate>
    {
        public static string QueryString => GetLoadQuery("gameobject_template");

        [DatabaseColumn("entry")]
        public uint Entry { get; set; }

        [DatabaseColumn("displayId")]
        public uint ModelId { get; set; }

        [DatabaseColumn("name")]
        public string Name { get; set; }

        [DatabaseColumn("size")]
        public float Scale { get; set; }
    }
}
