﻿using SharpDX;
using WoWEditor6.Storage.Database;

namespace WoWEditor6.Scene.Entities.GameObject
{
    class GameObjectSpawn : DatabaseLoader<GameObjectSpawn>
    {
        public static string QueryString => GetLoadQuery("gameobject");

        [DatabaseColumn("guid")]
        public ulong Guid { get; set; }

        [DatabaseColumn("id")]
        public uint Entry { get; set; }

        [DatabaseColumn("map")]
        public uint Map { get; set; }

        [DatabaseColumn("position_x")]
        public float X { get; set; }

        [DatabaseColumn("position_y")]
        public float Y { get; set; }

        [DatabaseColumn("position_z")]
        public float Z { get; set; }

        [DatabaseColumn("rotation0")]
        public float RotX { get; set; }

        [DatabaseColumn("rotation1")]
        public float RotY { get; set; }

        [DatabaseColumn("rotation2")]
        public float RotZ { get; set; }

        [DatabaseColumn("rotation3")]
        public float RotW { get; set; }

        public Quaternion Rotation { get { return new Quaternion(RotX, RotY, RotZ, RotW); } }

        public Vector3 Position { get { return new Vector3(X, Y, Z); } }

        public GameObjectTemplate Template { get { return WorldFrame.Instance.GameObjectManager.Templates[(int)Entry]; } }
    }
}
