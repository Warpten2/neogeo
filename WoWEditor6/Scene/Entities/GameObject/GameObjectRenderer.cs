﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWEditor6.IO.Files.Models;
using WoWEditor6.Scene.Models.M2;

namespace WoWEditor6.Scene.Entities.GameObject
{
    class GameObjectRenderer : M2Renderer
    {
        public GameObjectRenderer(M2File model) : base(model)
        {
        }

        public new void RenderBatch()
        {
            if (VisibleInstances.Count == 0)
                return;

            if (mIsSyncLoaded == false)
            {
                if (!BeginSyncLoad())
                    return;
            }

            if (mSkipRendering || Model.NeedsPerInstanceAnimation)
                return;

            if (Animator.GetBones(mAnimationMatrices))
                AnimBuffer.UpdateData(mAnimationMatrices);

            if (!Model.HasOpaquePass)
                return;

            mBatchRenderer.OnFrame(this);
        }

        public new GameObjectRenderInstance AddInstance(int uuid, Vector3 position, Vector3 rotation, Vector3 scaling)
        {
            M2RenderInstance inst;
            // ReSharper disable once InconsistentlySynchronizedField
            if (mFullInstances.TryGetValue(uuid, out inst))
            {
                ++inst.NumReferences;
                return (GameObjectRenderInstance)inst;
            }

            var instance = new GameObjectRenderInstance(uuid, position, rotation, scaling, this);
            lock (mFullInstances)
            {
                mFullInstances.Add(uuid, instance);
                if (!instance.IsVisible(WorldFrame.Instance.ActiveCamera))
                    return instance;

                lock (VisibleInstances)
                    VisibleInstances.Add(instance);
                return instance;
            }
        }
    }
}
