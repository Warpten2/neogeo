﻿using SharpDX;
using WoWEditor6.Scene.Models.M2;

namespace WoWEditor6.Scene.Entities.GameObject
{
    class GameObjectRenderInstance : M2RenderInstance
    {
        private GameObjectTemplate mTemplate;

        public GameObjectTemplate Template
        {
            get { return mTemplate; }
            set { mTemplate = value; UpdateModelNameplate(); }
        }

        public GameObjectRenderInstance(int uuid, Vector3 position, Vector3 rotation, Vector3 scale, M2Renderer renderer)
            : base(uuid, position, rotation, scale, renderer)
        {

        }

        public override void CreateModelNameplate()
        {
            if (mWorldModelName != null || mTemplate == null)
                return;

            mWorldModelName = new WorldText
            {
                Text = Template.Name,
                Scaling = 1.0f,
                DrawMode = WorldText.TextDrawMode.TextDraw3D
            };

            UpdateModelNameplate();
            WorldFrame.Instance.WorldTextManager.AddText(mWorldModelName);
        }

        protected override void UpdateModelNameplate()
        {
            if (mWorldModelName != null)
                mWorldModelName.Text = Template.Name;
            base.UpdateModelNameplate();
        }
    }
}
