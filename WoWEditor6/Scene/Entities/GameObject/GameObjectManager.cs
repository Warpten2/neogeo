﻿using System.Collections.Generic;
using SharpDX;
using WoWEditor6.IO.Files.Models;
using WoWEditor6.Storage;
using System.Data;
using System.Windows.Forms;
using WoWEditor6.Scene.Models;
using System;
using WoWEditor6.UI;
using WoWEditor6.Storage.Database;
using System.Threading.Tasks;
using System.Linq;
using WoWEditor6.Scene.Models.M2;
using WoWEditor6.Utils;

namespace WoWEditor6.Scene.Entities.GameObject
{
    class GameObjectManager : M2Manager
    {
        private List<M2Instance> _gameobjectInstances = new List<M2Instance>();

        private readonly Dictionary<int, string> _modelNameCache = new Dictionary<int, string>();

        /// <summary>
        /// A collection of all the templates loaded from the database.
        /// </summary>
        public Dictionary<int, GameObjectTemplate> Templates { get; private set; } = new Dictionary<int, GameObjectTemplate>();

        /// <summary>
        /// A collection of all the spawns loaded from the database for the current map.
        /// </summary>
        public Dictionary<ulong, GameObjectSpawn> Spawns { get; private set; } = new Dictionary<ulong, GameObjectSpawn>();

        public delegate void NullArgEventHandler();
        public event NullArgEventHandler OnTemplatesLoaded;
        public event NullArgEventHandler OnGameObjectInstanceSelected;

        public int GUIDGenerator { get; set; } = 0;

        /// <summary>
        /// Represents the creature spawn that has been selected for edition.
        /// </summary>
        private GameObjectRenderInstance _selectedGameObjectInstance;
        public GameObjectRenderInstance SelectedInstance
        {
            get { return _selectedGameObjectInstance; }
            set
            {
                _selectedGameObjectInstance = value;
                OnGameObjectInstanceSelected?.Invoke();
            }
        }

        public GameObjectTemplate SelectedTemplate { get; private set; } = null;

        public GameObjectManager() : base()
        {
        }

        public GameObjectRenderInstance GetGameObjectInstance(int uuid)
        {
            return _gameobjectInstances.FirstOrDefault(i => i.Uuid == uuid).RenderInstance as GameObjectRenderInstance;
        }

        public void Initialize(RenderControl window)
        {
            window.KeyUp += (s, e) =>
            {
                if (e.KeyCode == Keys.Delete)
                    DeleteSelectedInstance();
            };
            WorldFrame.Instance.OnMapLoaded += OnMapLoaded;
        }

        public void DeleteSelectedInstance()
        {
            if (SelectedInstance != null)
            {
                SelectedInstance.DestroyModelNameplate();
                RemoveInstance(SelectedInstance.Model.FileName, SelectedInstance.Uuid);
            }
        }

        /// <summary>
        /// Called when the world loads. Proceeds to loading all the GameObject templates
        /// and all those spawned on that map.
        /// </summary>
        /// <param name="mapId"></param>
        public void OnMapLoaded(int mapId)
        {
            Spawns.Clear();

            if (!MySqlConnector.Instance.CheckConnection())
            {
                Log.Error("Unable to load spawns - You should probably connect to the database!");
                return;
            }

            LoadSpawns(MySqlConnector.Instance.QueryToDataTable($"SELECT guid, id, map, position_x, position_y, position_z, rotation0, rotation1, rotation2, rotation3 FROM gameobject g LEFT JOIN gameobject_template gt ON gt.displayId != 0 && gt.entry = g.id WHERE map = {mapId}"));

            if (Templates.Count == 0)
                LoadTemplates(MySqlConnector.Instance.QueryToDataTable(@"SELECT entry, displayId, name, size FROM gameobject_template WHERE displayId != 0"));

            foreach (var spawn in Spawns)
            {
                var position = spawn.Value.Position.ToWorldCoordinates();
                var quaternion = spawn.Value.Rotation;
                SpawnGameObject(spawn.Value.Template, ref position, ref quaternion);
            }
            SelectTemplate(null);
        }

        /// <summary>
        /// Loads gameobject templates from the database.
        /// </summary>
        /// <param name="data"></param>
        public void LoadTemplates(DataTable data)
        {
            Parallel.ForEach(data.Rows.Cast<DataRow>(), (row) =>
            {
                var o = new GameObjectTemplate();
                o.Load(row);
                lock (Templates)
                    Templates.Add((int)o.Entry, o);
            });

            OnTemplatesLoaded?.Invoke();
        }

        /// <summary>
        /// Loads spawns for the current map from the database.
        /// </summary>
        /// <param name="data"></param>
        public void LoadSpawns(DataTable data)
        {
            Parallel.ForEach(data.Rows.Cast<DataRow>(), (row) =>
            {
                var gameobjectSpawn = new GameObjectSpawn();
                gameobjectSpawn.Load(row);
                lock (Spawns)
                    Spawns.Add(gameobjectSpawn.Guid, gameobjectSpawn);
            });
        }

        /// <summary>
        /// Called when a gameobject to spawn is selected in the GUI.
        /// </summary>
        /// <param name="template"></param>
        public void SelectTemplate(GameObjectTemplate template)
        {
            SelectedTemplate = template;
        }

        public void SelectTemplate(int templateId) => SelectTemplate(Templates[templateId]);

        /// <summary>
        /// Calculates intersections with raycasts to determine if an NPC has been clicked.
        /// </summary>
        public override void Intersect(IntersectionParams parameters)
        {
            if (mVisibleInstances == null || mNonBatchedInstances == null || mSortedInstances == null)
                return;

            var globalRay = Picking.Build(ref parameters.ScreenPosition, ref parameters.InverseView,
                ref parameters.InverseProjection);

            var minDistance = float.MaxValue;
            M2RenderInstance selectedInstance = null;

            lock (mVisibleInstances)
            {
                foreach (var pair in mVisibleInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            lock (mNonBatchedInstances)
            {
                foreach (var pair in mNonBatchedInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            lock (mSortedInstances)
            {
                foreach (var pair in mSortedInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            if (selectedInstance != null)
            {
                parameters.GameObjectInstance = selectedInstance as GameObjectRenderInstance;
                parameters.GameObjectModel = parameters.GameObjectInstance.Model;
                parameters.GameObjectPosition = globalRay.Position + minDistance * globalRay.Direction;
                parameters.GameObjectDistance = minDistance;
            }

            parameters.GameObjectHit = selectedInstance != null;
        }

        /// <summary>
        /// Handler for terrain click events after all rays have been cast.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="mouseEvent"></param>
        public void OnTerrainClicked(IntersectionParams intersection, MouseEventArgs mouseArgs)
        {
            if (SelectedTemplate == null)
                return;

            if (intersection.GameObjectHit)
            {
                SelectedInstance = intersection.GameObjectInstance;
            }
            else
            {
                if (mouseArgs.Button != MouseButtons.Left)
                    return;

                var intersectionPosition = Vector3.Zero;
                if (intersection.WmoHit)
                    intersectionPosition = intersection.WmoPosition;
                else if (intersection.TerrainHit)
                    intersectionPosition = intersection.TerrainPosition;

                var quat = Quaternion.Zero;
                if (intersection.WmoHit || intersection.TerrainHit)
                    Spawn(ref intersectionPosition, ref quat);
            }
        }

        public override void OnFrame(Camera activeCamera)
        {
            WorldFrame.Instance.M2Manager.PushMapReferences(_gameobjectInstances.Where(instance =>
            {
                var bbox = instance.BoundingBox;

                // This is the only camera type that exists
                // ReSharper disable once PossibleNullReferenceException
                var maxDistance = (activeCamera as PerspectiveCamera).FarClip;
                maxDistance *= maxDistance;

                // For clarity
                // ReSharper disable once ConvertIfStatementToReturnStatement
                if (Vector3.DistanceSquared(instance.RenderInstance.Position, activeCamera.Position) > maxDistance)
                    return false;

                return activeCamera.Contains(ref bbox);
            }).ToArray());

            base.OnFrame(activeCamera);
        }

        public bool SpawnGameObject(GameObjectTemplate template, ref Vector3 position, ref Quaternion orientation)
        {
            SelectTemplate(template);
            return Spawn(ref position, ref orientation);
        }

        public bool SpawnGameObject(int entry, Vector3 position, ref Quaternion orientation)
        {
            SelectTemplate(entry);
            return Spawn(ref position, ref orientation);
        }

        /// <summary>
        /// Actual spawning logic. Called when loading spawns into the world, or when the user clicks in world after having selected a template.
        /// </summary>
        /// <param name="position">Where to spawn the GameObject to.</param>
        /// <param name="orientation">Which direction the GameObject should be facing.</param>
        private bool Spawn(ref Vector3 position, ref Quaternion orientation)
        {
            string model;
            if (!_modelNameCache.TryGetValue((int)SelectedTemplate.ModelId, out model))
            {
                var gameobjectDisplayInfoEntry = DbcStorage.GameObjectDisplayInfo[(int)SelectedTemplate.ModelId];
                model = gameobjectDisplayInfoEntry.FileData.Value.CompletePath;
                _modelNameCache[(int)SelectedTemplate.ModelId] = model;
            }

            var gameObject = AddInstance(SelectedTemplate, model, position, orientation);
            if (gameObject == null)
                return false;

            var instance = new M2Instance
            {
                Hash = gameObject.Model.FileName.ToUpperInvariant().GetHashCode(),
                Uuid = gameObject.Uuid,
                BoundingBox = gameObject.BoundingBox,
                RenderInstance = gameObject,
                IsGameObject = true
            };

            _gameobjectInstances.Add(instance);

            PushMapReferences(new[] { instance });

            ++GUIDGenerator;
            return true;
        }

        private GameObjectRenderInstance AddInstance(GameObjectTemplate template, string model, Vector3 position, Quaternion orientation)
        {
            var hash = model.ToUpperInvariant().GetHashCode();
            lock (mRenderer)
            {
                M2Renderer renderer;
                if (mRenderer.TryGetValue(hash, out renderer))
                {
                    var creatureRenderer = renderer as GameObjectRenderer;
                    if (creatureRenderer != null)
                    {
                        var inst = creatureRenderer.AddInstance(GUIDGenerator, position, orientation.Axis, new Vector3(template.Scale));
                        inst.Template = template;
                        return inst;
                    }
                }

                var file = LoadModel<IO.Files.Models.WoD.M2File>(model);
                if (file == null)
                    return null;

                var render = new GameObjectRenderer(file);
                lock (mAddLock)
                    mRenderer.Add(hash, render);

                var instance = render.AddInstance(GUIDGenerator, position, orientation.Axis, new Vector3(template.Scale));
                instance.Template = template;
                return instance;
            }
        }

        /// <summary>
        /// Removes a gameobject spawn based on the model name (full path), and its GUID.
        /// </summary>
        /// <param name="model">The full path to the model name.</param>
        /// <param name="uuid">The creature's GUID.</param>
        public new void RemoveInstance(string model, int uuid)
        {
            var hash = model.ToUpperInvariant().GetHashCode();
            RemoveInstance(hash, uuid);
        }

        private new void RemoveInstance(int hash, int uuid)
        {
            if (mRenderer == null || mAddLock == null || mSortedInstances == null || mNonBatchedInstances == null ||
                mVisibleInstances == null)
                return;

            lock (mRenderer)
            {
                lock (mAddLock)
                {
                    mSortedInstances.Remove(uuid);
                    mNonBatchedInstances.Remove(uuid);
                    mVisibleInstances.Remove(uuid);
                }

                M2Renderer renderer;
                if (!mRenderer.TryGetValue(hash, out renderer))
                    return;

                if (renderer.RemoveInstance(uuid))
                {
                    lock (mAddLock)
                        mRenderer.Remove(hash);

                    lock (mUnloadList)
                        mUnloadList.Add(renderer);
                }
            }
        }
    }
}

