﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WoWEditor6.IO.Files.Models;
using WoWEditor6.Scene.Models;
using WoWEditor6.Scene.Models.M2;
using WoWEditor6.Storage;
using WoWEditor6.Storage.Database;
using WoWEditor6.UI;
using WoWEditor6.Utils;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureManager : M2Manager
    {
        private List<M2Instance> _creatureInstances { get; set; } = new List<M2Instance>();

        private readonly Dictionary<int, string> _modelNameCache = new Dictionary<int, string>();
        private readonly Dictionary<string, CreatureModelFile> _modelCache = new Dictionary<string, CreatureModelFile>();

        /// <summary>
        /// A collection of all the templates loaded from the database.
        /// </summary>
        public Dictionary<int, CreatureTemplate> Templates { get; private set; } = new Dictionary<int, CreatureTemplate>();

        /// <summary>
        /// A collection of all the spawns loaded from the database for the current map.
        /// </summary>
        public Dictionary<ulong, CreatureSpawn> Spawns { get; private set; } = new Dictionary<ulong, CreatureSpawn>();

        /// <summary>
        /// Represents the creature spawn that has been selected for edition.
        /// </summary>
        private CreatureRenderInstance _selectedCreatureInstance;
        public CreatureRenderInstance SelectedInstance
        {
            get { return _selectedCreatureInstance; }
            set
            {
                _selectedCreatureInstance = value;
                OnCreatureInstanceSelected?.Invoke();
            }
        }
        
        /// <summary>
        /// Represents the creature template selected in the GUI.
        /// NOT TO BE CONFUSED with the currently selected creature's template.
        /// </summary>
        public CreatureTemplate SelectedCreatureTemplate { get; private set; } = null;

        public delegate void NullArgEventHandler();
        public event NullArgEventHandler OnCreatureInstanceSelected;
        public event NullArgEventHandler OnTemplatesLoaded;

        /// <summary>
        /// Orientation defined in the GUI, used when manually spawning NPCs.
        /// </summary>
        private Vector3 _orientation = Vector3.Zero;

        /// <summary>
        /// A simple GUID generator for the map. Unrelated to database GUID.
        /// </summary>
        public int UuidGenerator { get; set; } = 0;

        public CreatureManager() : base()
        {
        }

        public CreatureRenderInstance GetCreatureInstance(int uuid)
        {
            return _creatureInstances.FirstOrDefault(i => i.Uuid == uuid).RenderInstance as CreatureRenderInstance;
        }

        public void Initialize(RenderControl window)
        {
            window.KeyUp += (s, e) =>
            {
                if (e.KeyCode == Keys.Delete)
                    DeleteSelectedInstance();
            };
            WorldFrame.Instance.OnMapLoaded += OnMapLoaded;
        }

        public void DeleteSelectedInstance()
        {
            if (SelectedInstance != null)
            {
                SelectedInstance.DestroyModelNameplate();
                RemoveInstance(SelectedInstance.Model.FileName, SelectedInstance.Uuid);
            }
        }

        /// <summary>
        /// Called when the world loads. Proceeds to loading all the NPC templates
        /// and all creatures spawned on that map.
        /// </summary>
        /// <param name="mapId"></param>
        public void OnMapLoaded(int mapId)
        {
            Spawns.Clear();

            if (!MySqlConnector.Instance.CheckConnection())
            {
                Log.Error("Unable to load spawns - You should probably connect to the database!");
                return;
            }

            LoadSpawns(MySqlConnector.Instance.QueryToDataTable($"SELECT guid, id, map, position_x, position_y, position_z, orientation, modelid FROM creature WHERE map = {mapId}"));

            if (Templates.Count == 0)
                LoadTemplates(MySqlConnector.Instance.QueryToDataTable(
                    @"SELECT entry, difficulty_entry_1, difficulty_entry_2, difficulty_entry_3,
                        KillCredit1, KillCredit2, modelid1, modelid2, modelid3, modelid4,
                        name, type, femaleName, subName, minlevel, maxlevel, ct.npcflag, speed_walk, speed_run, ct.scale
                        FROM creature_template ct"));

            foreach (var spawn in Spawns)
            {
                var position = spawn.Value.Position.ToWorldCoordinates();
                SpawnCreature(spawn.Value.Template, ref position, (spawn.Value.Orientation - 90.0f).ToDegree(), (int)spawn.Value.ModelId);
            }
            SelectTemplate(null);
        }

        /// <summary>
        /// Loads creature templates from the database.
        /// </summary>
        /// <param name="data"></param>
        public void LoadTemplates(DataTable data)
        {
            Parallel.ForEach(data.Rows.Cast<DataRow>(), (row) =>
            {
                var o = new CreatureTemplate();
                o.Load(row);
                lock (Templates)
                    Templates.Add((int)o.Entry, o);
            });

            OnTemplatesLoaded?.Invoke();
        }

        /// <summary>
        /// Loads spawns for the current map from the database.
        /// </summary>
        /// <param name="data"></param>
        public void LoadSpawns(DataTable data)
        {
            Parallel.ForEach(data.Rows.Cast<DataRow>(), (row) =>
            {
                var creatureSpawn = new CreatureSpawn();
                creatureSpawn.Load(row);
                lock (Spawns)
                    Spawns.Add(creatureSpawn.GUID, creatureSpawn);
            });
        }

        /// <summary>
        /// Called when a creature to spawn is selected in the GUI.
        /// </summary>
        /// <param name="template"></param>
        public void SelectTemplate(CreatureTemplate template)
        {
            SelectedCreatureTemplate = template;
        }

        public void SelectTemplate(int templateId) => SelectTemplate(Templates[templateId]);

        /// <summary>
        /// Calculates intersections with raycasts to determine if an NPC has been clicked.
        /// </summary>
        public override void Intersect(IntersectionParams parameters)
        {
            if (mVisibleInstances == null || mNonBatchedInstances == null || mSortedInstances == null)
                return;

            var globalRay = Picking.Build(ref parameters.ScreenPosition, ref parameters.InverseView,
                ref parameters.InverseProjection);

            var minDistance = float.MaxValue;
            M2RenderInstance selectedInstance = null;

            lock (mVisibleInstances)
            {
                foreach (var pair in mVisibleInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            lock (mNonBatchedInstances)
            {
                foreach (var pair in mNonBatchedInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            lock (mSortedInstances)
            {
                foreach (var pair in mSortedInstances)
                {
                    float dist;
                    if (pair.Value.Intersects(parameters, ref globalRay, out dist) && dist < minDistance)
                    {
                        minDistance = dist;
                        selectedInstance = pair.Value;
                    }
                }
            }

            if (selectedInstance != null)
            {
                parameters.CreatureInstance = selectedInstance as CreatureRenderInstance;
                parameters.CreatureModel = parameters.CreatureInstance.Model;
                parameters.CreaturePosition = globalRay.Position + minDistance * globalRay.Direction;
                parameters.CreatureDistance = minDistance;
            }

            parameters.CreatureHit = selectedInstance != null;
        }

        /// <summary>
        /// Handler for terrain click events after all rays have been cast.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="mouseEvent"></param>
        public void OnTerrainClicked(IntersectionParams intersection, MouseEventArgs mouseEvent)
        {
            if (SelectedCreatureTemplate == null)
                return;

            if (intersection.CreatureHit)
            {
                // If we hit a creature, it's already highlighted, so select it in data set.
                SelectedInstance = intersection.CreatureInstance;
            }
            else
            {
                if (mouseEvent.Button != MouseButtons.Left)
                    return;

                var intersectionPosition = Vector3.Zero;
                if (intersection.WmoHit)
                    intersectionPosition = intersection.WmoPosition;
                else if (intersection.TerrainHit)
                    intersectionPosition = intersection.TerrainPosition;

                // Terrain or wmo hit; spawn npc at collision coordinates
                if (intersection.WmoHit || intersection.TerrainHit)
                    Spawn(ref intersectionPosition, _orientation.Z);
            }
        }

        /// <summary>
        /// Sets the orientation of the npc to be spawned, and of the npc currently selected if any.
        /// </summary>
        /// <param name="degrees"></param>
        public void SetInstanceOrientation(float degrees)
        {
            _orientation.Z = degrees;
            if (SelectedInstance != null)
                SelectedInstance.Rotate(0, 0, degrees);
        }

        /// <summary>
        /// Called on every draw. Pushes NPCs to the drawable scene if they are visible by the camera (excluding raycasts).
        /// </summary>
        /// <param name="activeCamera"></param>
        public override void OnFrame(Camera activeCamera)
        {
            PushMapReferences(_creatureInstances.Where(instance =>
            {
                var bbox = instance.BoundingBox;

                // This is the only camera type that exists
                // ReSharper disable once PossibleNullReferenceException
                var maxDistance = (activeCamera as PerspectiveCamera).FarClip;
                maxDistance *= maxDistance;

                // For clarity
                // ReSharper disable once ConvertIfStatementToReturnStatement
                if (Vector3.DistanceSquared(instance.RenderInstance.Position, activeCamera.Position) > maxDistance)
                    return false;
                return activeCamera.Contains(ref bbox);
            }).ToArray());

            lock (mAddLock)
            {
                M2BatchRenderer.BeginDraw();
                // First draw all the instance batches
                foreach (var renderer in mRenderer.Values.Cast<CreatureRenderer>())
                    renderer.RenderBatch();

                M2SingleRenderer.BeginDraw();
                // Now draw those objects that need per instance animation
                foreach (var instance in mNonBatchedInstances.Values.Cast<CreatureRenderInstance>())
                {
                    instance.Model.LoadTextures(instance.InstanceData.DisplayInfo);
                    instance.Model.FilterMeshes(instance.InstanceData.DisplayInfo);
                    instance.Renderer.RenderSingleInstance(instance);
                }

                // Then draw those that have alpha blending and need ordering
                foreach (var instance in mSortedInstances.Values.Cast<CreatureRenderInstance>())
                {
                    instance.Model.LoadTextures(instance.InstanceData.DisplayInfo);
                    instance.Model.FilterMeshes(instance.InstanceData.DisplayInfo);
                    instance.Renderer.RenderSingleInstance(instance);
                }
            }

            IsViewDirty = false;
        }

        /// <summary>
        /// Spawns a specified template with the given parameters.
        /// </summary>
        public bool SpawnCreature(CreatureTemplate template, ref Vector3 position, float orientation, int modelId = -1)
        {
            SelectTemplate(template);
            return Spawn(ref position, orientation, modelId);
        }

        /// <summary>
        /// Spawns a specified template with the given parameters.
        /// </summary>
        public bool SpawnCreature(int entry, Vector3 position, float orientation, int modelId = -1)
        {
            SelectTemplate(entry);
            return Spawn(ref position, orientation, modelId);
        }

        /// <summary>
        /// Actual spawning logic. Called when loading spawns into the world, or when the user clicks in world after having selected a template.
        /// </summary>
        /// <param name="position">Where to spawn the NPC to.</param>
        /// <param name="orientation">Which direction the NPC should be facing.</param>
        /// <param name="modelId">Optional. Forces a modelId. Used by the database loader.</param>
        private bool Spawn(ref Vector3 position, float orientation, int modelId = -1)
        {
            if (SelectedCreatureTemplate == null)
                return false;

            string model = string.Empty;
            if (modelId <= 0)
                modelId = (int)SelectedCreatureTemplate.ModelIds.RandomElement(i => i != 0);

            if (!_modelNameCache.TryGetValue(modelId, out model))
            {
                var displayInfoEntry = DbcStorage.CreatureDisplayInfo[modelId];
                if (displayInfoEntry == null)
                    return false;

                model = displayInfoEntry.ModelData.Value.ModelPath;
                _modelNameCache[modelId] = model;
            }

            var creature = AddInstance(SelectedCreatureTemplate, model, position, orientation, modelId);
            if (creature == null)
                return false;

            var instance = new M2Instance
            {
                Hash = creature.Model.FileName.ToUpperInvariant().GetHashCode(),
                Uuid = creature.Uuid,
                BoundingBox = creature.BoundingBox,
                RenderInstance = creature,
                IsCreature = true
            };

            _creatureInstances.Add(instance);

            PushMapReference(instance);

            ++UuidGenerator;
            return true;
        }

        private CreatureRenderInstance AddInstance(CreatureTemplate template, string model, Vector3 position, float orientation, int modelId)
        {
            var hash = model.ToUpperInvariant().GetHashCode();
            lock (mRenderer)
            {
                M2Renderer renderer;
                if (mRenderer.TryGetValue(hash, out renderer))
                {
                    var creatureRenderer = renderer as CreatureRenderer;
                    if (creatureRenderer != null)
                    {
                        var inst = creatureRenderer.AddInstance(UuidGenerator, position, new Vector3(0.0f, 0.0f, orientation), new Vector3(template.Scale));
                        inst.InstanceData = creatureRenderer.Model.GetCreatureInstanceData(modelId);
                        inst.Template = template;
                        return inst;
                    }
                }

                var file = LoadModel<CreatureModelFile>(model);
                if (file == null)
                    return null;

                var render = new CreatureRenderer(file);
                render.Model.LoadTextures(modelId);
                render.Model.FilterMeshes(modelId);
                lock (mAddLock)
                    mRenderer.Add(hash, render);

                var instance = render.AddInstance(UuidGenerator, position, new Vector3(0.0f, 0.0f, orientation), new Vector3(template.Scale));
                instance.InstanceData = render.Model.GetCreatureInstanceData(modelId);
                instance.Template = template;
                return instance;
            }
        }

        /// <summary>
        /// Removes a creature spawn based on the model name (full path), and its GUID.
        /// </summary>
        /// <param name="model">The full path to the model name.</param>
        /// <param name="uuid">The creature's GUID.</param>
        public new void RemoveInstance(string model, int uuid)
        {
            var hash = model.ToUpperInvariant().GetHashCode();
            RemoveInstance(hash, uuid);
        }

        private new void RemoveInstance(int hash, int uuid)
        {
            if (mRenderer == null || mAddLock == null || mSortedInstances == null || mNonBatchedInstances == null ||
                mVisibleInstances == null)
                return;

            lock (mRenderer)
            {
                lock (mAddLock)
                {
                    mSortedInstances.Remove(uuid);
                    mNonBatchedInstances.Remove(uuid);
                    mVisibleInstances.Remove(uuid);
                }

                M2Renderer renderer;
                if (!mRenderer.TryGetValue(hash, out renderer))
                    return;

                if (renderer.RemoveInstance(uuid))
                {
                    lock (mAddLock)
                        mRenderer.Remove(hash);

                    lock (mUnloadList)
                        mUnloadList.Add(renderer);
                }
            }
        }
    }
}
