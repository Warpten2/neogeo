﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using WoWEditor6.IO.Files.Models;
using WoWEditor6.IO.Files.Structures;
using WoWEditor6.Scene.Models.M2;
using WoWEditor6.Scene.Texture;
using WoWEditor6.Storage;
using WoWEditor6.Utils;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureRenderer : M2Renderer
    {
        public new CreatureModelFile Model { get { return base.Model as CreatureModelFile; } }

        public CreatureRenderer(CreatureModelFile model) : base(model)
        {
        }

        public new void RenderBatch()
        {
            if (VisibleInstances.Count == 0)
                return;

            if (mIsSyncLoaded == false)
            {
                if (!BeginSyncLoad())
                    return;
            }

            if (mSkipRendering || Model.NeedsPerInstanceAnimation)
                return;

            if (Animator.GetBones(mAnimationMatrices))
                AnimBuffer.UpdateData(mAnimationMatrices);

            if (!Model.HasOpaquePass)
                return;

            var renderInstance = VisibleInstances.Cast<CreatureRenderInstance>().First();
            Model.LoadTextures(renderInstance.InstanceData.DisplayInfo);
            Model.FilterMeshes(renderInstance.InstanceData.DisplayInfo);
            mBatchRenderer.OnFrame(this);
        }

        public new CreatureRenderInstance AddInstance(int uuid, Vector3 position, Vector3 rotation, Vector3 scaling)
        {
            M2RenderInstance inst;
            // ReSharper disable once InconsistentlySynchronizedField
            if (mFullInstances.TryGetValue(uuid, out inst))
            {
                ++inst.NumReferences;
                return (CreatureRenderInstance)inst;
            }

            var instance = new CreatureRenderInstance(uuid, position, rotation, scaling, this);
            lock (mFullInstances)
            {
                mFullInstances.Add(uuid, instance);
                if (!instance.IsVisible(WorldFrame.Instance.ActiveCamera))
                    return instance;

                lock (VisibleInstances)
                    VisibleInstances.Add(instance);
                return instance;
            }
        }
    }
}
