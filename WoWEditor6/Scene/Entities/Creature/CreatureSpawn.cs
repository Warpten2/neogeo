﻿using SharpDX;
using WoWEditor6.Storage.Database;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureSpawn : DatabaseLoader<CreatureSpawn>
    {
        public static string QueryString => GetLoadQuery("gameobject");

        [DatabaseColumn("guid")]
        public ulong GUID { get; set; }

        [DatabaseColumn("id")]
        public uint Entry { get; set; }

        [DatabaseColumn("map")]
        public uint Map { get; set; }

        [DatabaseColumn("position_x")]
        public float X { get; set; }

        [DatabaseColumn("position_y")]
        public float Y { get; set; }

        [DatabaseColumn("position_z")]
        public float Z { get; set; }

        [DatabaseColumn("orientation")]
        public float Orientation { get; set; }

        [DatabaseColumn("modelid")]
        public uint ModelId { get; set; }

        public Vector3 Position { get { return new Vector3(X, Y, Z); } }

        public CreatureTemplate Template { get { return WorldFrame.Instance.CreatureManager.Templates[(int)Entry]; } }
    }
}

