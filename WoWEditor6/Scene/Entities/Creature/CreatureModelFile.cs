﻿using System.Collections.Generic;
using System.Linq;
using WoWEditor6.IO.Files.Models.WoD;
using WoWEditor6.IO.Files.Structures;
using WoWEditor6.Storage;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureInstanceData
    {
        private static Dictionary<int, CreatureInstanceData> _data = new Dictionary<int, CreatureInstanceData>();

        public static bool TryGet(int modelId, out CreatureInstanceData creatureInstanceData) => _data.TryGetValue(modelId, out creatureInstanceData);

        public static void Clear() => _data.Clear();

        public static bool Create(int modelId, CreatureModelFile model, out CreatureInstanceData creatureData)
        {
            if (!DbcStorage.CreatureDisplayInfo.HasKey(modelId))
            {
                creatureData = null;
                return false;
            }

            var displayInfo = DbcStorage.CreatureDisplayInfo[modelId];
            var creatureModelDataEntry = displayInfo.ModelData.Value;

            if (creatureModelDataEntry == null)
            {
                creatureData = null;
                return false;
            }

            creatureData = new CreatureInstanceData();
            creatureData.DisplayInfo = displayInfo;

            // If cdie is null, that means the creature is just... without geoset. Let's not prevent data from being loaded in that case.
            var displayInfoExtra = displayInfo.ExtraDisplayInfo.Value;
            creatureData.DisplayInfoExtra = displayInfoExtra;
            creatureData.Race = 0;
            creatureData.Gender = CreatureGender.Neutral;
            if (displayInfoExtra != null)
            {
                creatureData.Race = displayInfoExtra.RaceID;
                creatureData.Gender = (CreatureGender)displayInfoExtra.Gender;
            }

            var charSections = DbcStorage.CharSections.Where(rec => {
                if (displayInfoExtra == null)
                    return rec.RaceID == 0 && rec.SexID == 2;

                return rec.RaceID == displayInfoExtra.RaceID && rec.SexID == displayInfoExtra.Gender;
            });

            var textureTypes = model.TextureInfos.Select(tex => tex.TextureType).Distinct().Where(i => i != 0);

            foreach (var textureType in textureTypes)
            {
                switch (textureType)
                {
                    case 1: // Body (Skin + Clothes)
                        if (creatureData.DisplayInfoExtra == null)
                            break;

                        var textureId = creatureData.DisplayInfoExtra.FileDataID == 0 ? creatureData.DisplayInfoExtra.HDFileDataID : creatureData.DisplayInfoExtra.FileDataID;
                        var fileData = DbcStorage.FileData[textureId];
                        creatureData.Textures.Add(1, Texture.TextureManager.Instance.GetTexture(fileData.CompletePath));
                        break;
                    case 2: // Cape
                        if (creatureData.DisplayInfoExtra == null || creatureData.DisplayInfoExtra.CapeID == 0)
                            break;

                        var itemDisplayInfo = DbcStorage.ItemDisplayInfo[creatureData.DisplayInfoExtra.CapeID];
                        if (itemDisplayInfo != null)
                            creatureData.Textures.Add(2, Texture.TextureManager.Instance.GetTexture(itemDisplayInfo.ModelTextureA));
                        break;
                    case 6: // Character Hair
                        var charHairEntry = charSections.FirstOrDefault(r => r.BaseSection == 3);
                        if (charHairEntry != null)
                            creatureData.Textures.Add(6, Texture.TextureManager.Instance.GetTexture(charHairEntry.Texture[0]));
                        break;
                    case 8: // Skin Extra
                        var skinExtraEntry = charSections.FirstOrDefault(r => r.BaseSection == 0);
                        if (skinExtraEntry != null)
                            creatureData.Textures.Add(8, Texture.TextureManager.Instance.GetTexture(skinExtraEntry.Texture[1]));
                        break;
                    case 11: // Monster Skin #1
                    case 12: // Monster Skin #2 
                    case 13: // Monster Skin #3
                        var skinString = displayInfo.TextureVariation[textureType - 11];
                        if (!string.IsNullOrEmpty(skinString))
                            creatureData.Textures.Add(textureType, Texture.TextureManager.Instance.GetTexture($"{model.ModelRoot}\\{skinString}.blp"));
                        break;
                    default:
                        // textureTable.Add(textureType, TextureManager.Instance.GetTexture("creaturecrappygreentexture.blp"));
                        break;
                }
            }

            _data[modelId] = creatureData;
            return true;
        }

        public enum CreatureGender : int
        {
            Male = 0,
            Female = 1,
            Neutral = 2
        }

        public Dictionary<int /* textureType */, Graphics.Texture> Textures { get; private set; }
        public int Race { get; set; }
        public CreatureGender Gender { get; set; }
        public CreatureDisplayInfoEntry DisplayInfo { get; set; }
        public CreatureDisplayInfoExtraEntry DisplayInfoExtra { get; set; }
        public CreatureTemplate Template { get; set; }

        public CreatureInstanceData()
        {
            Textures = new Dictionary<int, Graphics.Texture>();
            Race = 0;
            Gender = CreatureGender.Neutral;
            DisplayInfo = null;
            DisplayInfoExtra = null;
        }
    }

    class CreatureModelFile : M2File
    {
        public CreatureModelFile() : base() { }

        public void FilterMeshes(int displayId)
        {
            var creatureData = GetCreatureInstanceData(displayId);
            FilterMeshesByGeoset(creatureData.DisplayInfo.CreatureGeosetData, creatureData.DisplayInfoExtra);
        }

        public void FilterMeshes(CreatureDisplayInfoEntry entry) => FilterMeshes(entry.ID);

        public void LoadTextures(int displayId)
        {
            var creatureData = GetCreatureInstanceData(displayId);

            for (var i = 0; i < TextureInfos.Length; ++i)
                if (TextureInfos[i].TextureType != 0 && creatureData.Textures.ContainsKey(TextureInfos[i].TextureType))
                    TextureInfos[i].Texture = creatureData.Textures[TextureInfos[i].TextureType];
        }

        public void LoadTextures(CreatureDisplayInfoEntry entry) => LoadTextures(entry.ID);

        public CreatureInstanceData GetCreatureInstanceData(int displayId)
        {
            CreatureInstanceData data;
            if (!CreatureInstanceData.TryGet(displayId, out data))
                if (!CreatureInstanceData.Create(displayId, this, out data))
                    return null;
            return data;
        }

        /// <summary>
        /// DO NOT CALL THIS FUNCTION DIRECTLY UNLESS YOU KNOW WHAT YOU ARE DOING
        /// </summary>
        /// <param name="geosetMask"></param>
        /// <param name="creatureDisplayInfoExtra"></param>
        public override void FilterMeshesByGeoset(int geosetMask, CreatureDisplayInfoExtraEntry creatureDisplayInfoExtra)
        {
            if (creatureDisplayInfoExtra == null)
                return;

            // var enabledGroups = new int[8];
            // for (var i = 0; i < 8; ++i)
            //     enabledGroups[i] = (geosetMask & (0xF << (i * 4))) >> (i * 4);

            var enabledGeosetMeshIds = new Dictionary<int /* group */, int /* activeMeshId */>(20);
            enabledGeosetMeshIds.Add(0, 0); // Skin
            for (var i = 1; i <= 20; ++i)
                enabledGeosetMeshIds[i] = i * 100 + 1;

            var glovesDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.GlovesID];
            var chestDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.CuirassID];
            var shirtDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.ShirtID];
            var beltDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.BeltID];
            var pantsDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.LegsID];
            var bootsDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.BootsID];
            var cloakDisplayInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.CapeID];

            if (creatureDisplayInfoExtra.GlovesID != 0 && glovesDisplayInfo.GeosetGroup[0] != 0) // Gloves?
                enabledGeosetMeshIds[4] = glovesDisplayInfo.GeosetGroup[0] + 401;
            else if (creatureDisplayInfoExtra.CuirassID != 0 && chestDisplayInfo.GeosetGroup[0] != 0) // Nope. Chest?
                enabledGeosetMeshIds[8] = chestDisplayInfo.GeosetGroup[0] + 801;
            else if (creatureDisplayInfoExtra.ShirtID != 0 && shirtDisplayInfo.GeosetGroup[0] != 0) // Nope. Ok, shirt then.
                enabledGeosetMeshIds[8] = shirtDisplayInfo.GeosetGroup[0] + 801;

            var hasBulkyBeltFlag = beltDisplayInfo != null && beltDisplayInfo.GeosetGroup[0] != 0 &&
                (beltDisplayInfo.Flags & (1 << 9)) != 0;

            var dressPants = false;
            var dressChestpiece = false;

            if (creatureDisplayInfoExtra.CuirassID != 0 && chestDisplayInfo.GeosetGroup[2] != 0)
            {
                dressPants = false;
                dressChestpiece = true;
                enabledGeosetMeshIds[13] = chestDisplayInfo.GeosetGroup[2] + 1301;
            }
            else if (creatureDisplayInfoExtra.LegsID != 0 && pantsDisplayInfo.GeosetGroup[2] != 0 /* && characterComponent.field_20 & 0x08 */)
            {
                dressPants = true;
                dressChestpiece = false;
                enabledGeosetMeshIds[13] = pantsDisplayInfo.GeosetGroup[2] + 1301;
            }
            else
            {
                if (bootsDisplayInfo != null && bootsDisplayInfo.GeosetGroup[0] != 0)
                {
                    enabledGeosetMeshIds[9] = 901;
                    enabledGeosetMeshIds[5] = bootsDisplayInfo.GeosetGroup[0] + 501;
                }
                else if (creatureDisplayInfoExtra.LegsID != 0 /* && characterComponent.field_20 & 0x08 */)
                    enabledGeosetMeshIds[9] = pantsDisplayInfo.GeosetGroup[1] + 901;
            }

            if (creatureDisplayInfoExtra.BootsID != 0)
            {
                if (bootsDisplayInfo.GeosetGroup[1] == 0)
                    enabledGeosetMeshIds[20] = 2002;
                else
                    enabledGeosetMeshIds[20] = bootsDisplayInfo.GeosetGroup[1] + 2000;
            }
            else
                enabledGeosetMeshIds[20] = 2001;

            // Tabard here ...
            var showsTabard = false;
            var hasDress = (dressChestpiece | dressPants);
            if (!hasDress && !hasBulkyBeltFlag && creatureDisplayInfoExtra.TabardID != 0)
            {
                var tabardInfo = DbcStorage.ItemDisplayInfo[creatureDisplayInfoExtra.TabardID];
                if (creatureDisplayInfoExtra.TabardID != 0 && tabardInfo.GeosetGroup[0] != 0)
                {
                    showsTabard = true;
                    enabledGeosetMeshIds[12] = tabardInfo.GeosetGroup[0] + 1201;
                }
            }

            // if (characterComponent->field_20 & 0x20 == 0)
            // {
            //     CM2Model::SetGeometryVisible(ourModel, 1201, 1201, 1);
            // }
            // if ((characterComponent->field_20 & 0x20 == 0) && !hasDress)
            // {
            //     CM2Model::SetGeometryVisible(ourModel, 1202, 1202, 1);
            //     showsTabard = true;
            // }
            // else ...
            if (!showsTabard && !hasDress)
            {
                if (creatureDisplayInfoExtra.CuirassID != 0 && chestDisplayInfo.GeosetGroup[1] != 0)
                {
                    showsTabard = false;
                    enabledGeosetMeshIds[10] = chestDisplayInfo.GeosetGroup[1] + 1001;
                }
                else if (creatureDisplayInfoExtra.ShirtID != 0 && shirtDisplayInfo.GeosetGroup[1] != 0)
                {
                    showsTabard = false;
                    enabledGeosetMeshIds[10] = shirtDisplayInfo.GeosetGroup[1] + 1001;
                }
            }

            if (!dressChestpiece /* && (characterComponent->field_20 & 8 == 0)  */)
            {
                if (creatureDisplayInfoExtra.LegsID != 0 && pantsDisplayInfo.GeosetGroup[0] != 0)
                {
                    var geosetId = pantsDisplayInfo.GeosetGroup[0];
                    if (geosetId > 2)
                    {
                        enabledGeosetMeshIds.Remove(13);
                        enabledGeosetMeshIds[11] = 1101 + geosetId;
                    }
                    else if (showsTabard)
                        enabledGeosetMeshIds[11] = 1101 + geosetId;
                }
            }

            if (creatureDisplayInfoExtra.CapeID != 0 && cloakDisplayInfo.GeosetGroup[0] != 0)
            {
                enabledGeosetMeshIds[15] = 1501 + cloakDisplayInfo.GeosetGroup[0];
            }
            else
                enabledGeosetMeshIds[15] = 1501;

            if (creatureDisplayInfoExtra.BeltID != 0 && beltDisplayInfo.GeosetGroup[0] != 0)
            {
                enabledGeosetMeshIds[18] = 1801 + beltDisplayInfo.GeosetGroup[0];
            }

            // if (characterComponent->field_20 & 0x10 != 0)
            // {
            // If you aren't wearing PANTS, you have a tabard, and you don't have a dress (a dress from CHEST presumably, since you aren't wearing PANTS), enable 1401
            if (showsTabard && creatureDisplayInfoExtra.LegsID == 0 && !hasDress)
                enabledGeosetMeshIds[14] = 1401;
            else
                enabledGeosetMeshIds.Remove(14);
            // }

            // Removes unneeded edge and vertices to save gpu
            // CM2Model::OptimizeVisibleGeometry(ourModel);
            // FIELD 20 MODIFICATION (&= 0xFD)
            // characterComponent->field_20 &= 0xFD;
            foreach (var pass in Passes)
                pass.Enabled = enabledGeosetMeshIds.ContainsValue(pass.MeshId);
        }
    }
}
