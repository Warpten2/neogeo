﻿using SharpDX;
using WoWEditor6.Scene.Models.M2;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureRenderInstance : M2RenderInstance
    {
        public CreatureInstanceData InstanceData { get; set; }

        public new CreatureRenderer Renderer { get { return base.Renderer as CreatureRenderer; } }
        public new CreatureModelFile Model { get { return base.Model as CreatureModelFile; } }

        public CreatureTemplate Template
        {
            get { return InstanceData.Template; }
            set { InstanceData.Template = value; UpdateModelNameplate(); }
        }

        public CreatureRenderInstance(int uuid, Vector3 position, Vector3 rotation, Vector3 scale, CreatureRenderer renderer)
            : base(uuid, position, rotation, scale, renderer)
        {
            IsCreature = true;
        }

        public override void CreateModelNameplate()
        {
            if (mWorldModelName != null || Template == null)
                return;

            mWorldModelName = new WorldText
            {
                Text = Template.MaleName,
                Scaling = 1.0f,
                DrawMode = WorldText.TextDrawMode.TextDraw3D
            };

            UpdateModelNameplate();
            WorldFrame.Instance.WorldTextManager.AddText(mWorldModelName);
        }

        protected override void UpdateModelNameplate()
        {
            if (mWorldModelName != null)
                mWorldModelName.Text = Template.MaleName;
            base.UpdateModelNameplate();
        }
    }
}
