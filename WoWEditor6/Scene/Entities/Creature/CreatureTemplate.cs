﻿using WoWEditor6.Storage.Database;

namespace WoWEditor6.Scene.Entities.Creature
{
    class CreatureTemplate : DatabaseLoader<CreatureTemplate>
    {
        public static string QueryString => GetLoadQuery("creature_template");

        [DatabaseColumn("entry")]
        public uint Entry { get; set; }

        [DatabaseColumn("difficulty_entry_1", "difficulty_entry_2", "difficulty_entry_3")]
        public uint[] DifficultyEntries { get; set; }

        [DatabaseColumn("KillCredit1", "KillCredit2")]
        public uint[] KillCredits { get; set; }

        [DatabaseColumn("modelid1", "modelid2", "modelid3", "modelid4")]
        public uint[] ModelIds { get; set; }

        [DatabaseColumn("name")]
        public string MaleName { get; set; }

        [DatabaseColumn("type")]
        public int Type { get; set; }

        [DatabaseColumn("femaleName")]
        private string FemaleName { get; set; }

        [DatabaseColumn("subName")]
        private string SubName { get; set; }

        [DatabaseColumn("minlevel")]
        private int MinLevel { get; set; }

        [DatabaseColumn("maxlevel")]
        private int MaxLevel { get; set; }

        [DatabaseColumn("npcflag")]
        public ulong Flags { get; set; }

        [DatabaseColumn("speed_walk")]
        public float WalkSpeed { get; set; }

        [DatabaseColumn("speed_run")]
        public float RunSpeed { get; set; }

        [DatabaseColumn("scale")]
        public float Scale { get; set; }

        public static bool IsMale(int modelId)
        {
            if (!MySqlConnector.Instance.CheckConnection())
                return false;

            var dt = MySqlConnector.Instance.QueryToDataTable($"SELECT gender FROM creature_model_info WHERE modelid = {modelId}");
            if (dt.Rows.Count == 0)
                return false;
            return (byte)dt.Rows[0]["gender"] == 0;
        }

        public static bool IsFemale(int modelId)
        {
            if (!MySqlConnector.Instance.CheckConnection())
                return false;

            var dt = MySqlConnector.Instance.QueryToDataTable($"SELECT gender FROM creature_model_info WHERE modelid = {modelId}");
            if (dt.Rows.Count == 0)
                return false;
            return (byte)dt.Rows[0]["gender"] == 1;
        }

        public override string ToString()
        {
            return $"{MaleName} (#{Entry})";
        }
    }
}
