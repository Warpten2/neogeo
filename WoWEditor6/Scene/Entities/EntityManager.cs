﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WoWEditor6.Scene.Entities.Creature;
using WoWEditor6.Scene.Entities.GameObject;

namespace WoWEditor6.Scene.Entities
{
    class EntityManager : Singleton<EntityManager>
    {
        public CreatureManager CreatureMgr { get; private set; }
        public GameObjectManager GameObjectMgr { get; private set; }

        public CreatureTemplate GetCreatureTemplate(int entry) => CreatureMgr.Templates[entry];
        public CreatureTemplate GetCreatureTemplate(uint entry) => GetCreatureTemplate((int)entry);

        public GameObjectTemplate GetGameObjectTemplate(int entry) => GameObjectMgr.Templates[entry];
        public GameObjectTemplate GetGameObjectTemplate(uint entry) => GetGameObjectTemplate((int)entry);

        public CreatureRenderInstance GetCreature(int guid) => CreatureMgr.GetCreatureInstance(guid);
        public GameObjectRenderInstance GetGameObject(int guid) => GameObjectMgr.GetGameObjectInstance(guid);

        public void Intersect(IntersectionParams intersection)
        {
            CreatureMgr.Intersect(intersection);
            GameObjectMgr.Intersect(intersection);
        }

        public void OnFrame(Camera activeCamera)
        {
            CreatureMgr.OnFrame(activeCamera);
            GameObjectMgr.OnFrame(activeCamera);
        }

        public bool Spawn(CreatureTemplate template, ref Vector3 position, float orientation, int modelId = -1)
            => CreatureMgr.SpawnCreature(template, ref position, orientation, modelId);

        public bool Spawn(GameObjectTemplate template, ref Vector3 position, ref Quaternion orientations)
            => GameObjectMgr.SpawnGameObject(template, ref position, ref orientations);
    }
}
