﻿using System.Security.RightsManagement;
using SharpDX;
using WoWEditor6.IO.Files.Models;
using WoWEditor6.IO.Files.Terrain;
using WoWEditor6.Scene.Models.M2;
using WoWEditor6.Scene.Models.WMO;
using WoWEditor6.Scene.Entities.GameObject;
using WoWEditor6.Scene.Entities.Creature;

namespace WoWEditor6.Scene
{
    class IntersectionParams
    {
        public Vector3 TerrainPosition { get; set; }
        public float TerrainDistance { get; set; }
        public bool TerrainHit { get; set; }

        public bool M2Hit { get; set; }
        public M2File M2Model { get; set; }
        public M2RenderInstance M2Instance { get; set; }
        public Vector3 M2Position { get; set; }
        public float M2Distance { get; set; }

        public bool CreatureHit { get; set; }
        public CreatureRenderInstance CreatureInstance { get; set; }
        public CreatureModelFile CreatureModel { get; set; }
        public float CreatureDistance { get; set; }
        public Vector3 CreaturePosition { get; set; }

        public bool GameObjectHit { get; set; }
        public GameObjectRenderInstance GameObjectInstance { get; set; }
        public M2File GameObjectModel { get; set; }
        public Vector3 GameObjectPosition { get; set; }
        public float GameObjectDistance { get; set; }

        public bool WmoHit { get; set; }
        public WmoRoot WmoModel { get; set; }
        public WmoInstance WmoInstance { get; set; }
        public Vector3 WmoPosition { get; set; }
        public float WmoDistance { get; set; }

        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public MapChunk ChunkHit { get; set; }

        public Matrix InverseView;
        public Matrix InverseProjection;
        public Vector2 ScreenPosition;

        public IntersectionParams(Matrix inverseView, Matrix inverseProjection, Vector2 screenPosition)
        {
            InverseView = inverseView;
            InverseProjection = inverseProjection;
            ScreenPosition = screenPosition;
        }
    }
}
