﻿using System;
using System.Collections.Generic;

namespace WoWEditor6.IO.Files.Models
{
    class ModelFactory
    {
        public static ModelFactory Instance { get; private set; }

        private static Dictionary<string, M2File> mModelCache = new Dictionary<string, M2File>();

        static ModelFactory()
        {
            Instance = new ModelFactory();
        }

        public IM2Animator CreateAnimator(M2File file)
        {
            var wodModel = file as WoD.M2File;
            if (wodModel != null)
                return new WoD.M2Animator(wodModel);

            throw new NotImplementedException("Version not yet implemented for m2 animations");
        }

        public T CreateM2<T>(string file) where T : M2File, new()
        {
            M2File returnValue;
            if (mModelCache.TryGetValue(file, out returnValue))
                return (T)returnValue;

            var retValue = new T();
            retValue.Init(file);
            return retValue;
        }

        public WmoRoot CreateWmo()
        {
            return new WoD.WmoRoot();
        }
    }
}
