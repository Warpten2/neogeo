﻿using System.IO;
using SharpDX;

namespace WoWEditor6.IO.Files.Models.WoD
{
    class M2UVAnimation
    {
        private readonly M2Vector3AnimationBlock mTranslation;
        private readonly M2Quaternion16AnimationBlock mRotation;
        private readonly M2Vector3AnimationBlock mScaling;

        public M2UVAnimation(M2File file, ref M2TexAnim texAnim, BinaryReader reader)
        {
            mTranslation = new M2Vector3AnimationBlock(file, texAnim.translation, reader);
            mRotation = new M2Quaternion16AnimationBlock(file, texAnim.rotation, reader);
            mScaling = new M2Vector3AnimationBlock(file, texAnim.scaling, reader, Vector3.One);
        }

        public void UpdateMatrix(int animation, uint time, out Matrix matrix)
        {
            var position = mTranslation.GetValueDefaultLength(animation, time);
            var scaling = mScaling.GetValueDefaultLength(animation, time);
            var rotation = mRotation.GetValueDefaultLength(animation, time);

            /*matrix = Matrix.Translation(0.5f, 0.5f, 0.0f) * Matrix.Translation(position) * Matrix.RotationQuaternion(rotation) *
                Matrix.Scaling(scaling) * Matrix.Translation(-0.5f, -0.5f, 0.0f);*/

            matrix = Matrix.RotationQuaternion(rotation) * Matrix.Scaling(scaling) * Matrix.Translation(position);
        }
    }
}
