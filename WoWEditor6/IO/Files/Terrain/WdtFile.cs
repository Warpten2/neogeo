﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WoWEditor6.IO.Files.Terrain
{
    class WdtFile
    {
        public uint Flags { get; set; }
        public string GlobalWmo { get; private set; }
        public Modf MODF { get; private set; }
        public bool IsInstanceMap { get; private set; }

        public void Load(string continent)
        {
            var fileName = string.Format(@"World\Maps\{0}\{0}.wdt", continent);
            using (var strm = FileManager.Instance.Provider.OpenFile(fileName))
            {
                if (strm == null)
                {
                    Flags = 0;
                    return;
                }

                var reader = new BinaryReader(strm);
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    try
                    {
                        var id = reader.ReadUInt32();
                        var size = reader.ReadInt32();
                        switch (id)
                        {
                            case 0x4D504844: // MPHD
                                Flags = reader.ReadUInt32();
                                reader.BaseStream.Position += size - 4;
                                break;
                            case 0x4D574D4F: // MWMO
                                IsInstanceMap = true;
                                GlobalWmo = reader.ReadCString(0x100);
                                break;
                            case 0x4D4F4446: // MODF
                                IsInstanceMap = true;
                                MODF = reader.Read<Modf>();
                                break;
                            default:
                                reader.BaseStream.Position += size;
                                break;
                        }
                    }
                    catch  (Exception)
                    {
                        Flags = 0;
                        return;
                    }
                }
            }
        }
    }
}
