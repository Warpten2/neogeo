﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharpDX;
using WoWEditor6.Scene;

namespace WoWEditor6.IO.Files.Terrain
{
    abstract class MapChunk : IDisposable
    {
        protected Vector3 mMidPoint;
        protected float mMinHeight = float.MaxValue;
        protected float mMaxHeight = float.MinValue;
        protected bool mUpdateNormals;
        protected Mcly[] mLayers = new Mcly[0];
        protected bool mIsYInverted = false;

        public int IndexX { get; protected set; }
        public int IndexY { get; protected set; }

        public int StartVertex {get{return (IndexX + IndexY * 16) * 145;}}

        public AdtVertex[] Vertices { get; private set; }
        public uint[] AlphaValues { get; private set; }
        public byte[] HoleValues { get; private set; }
        public IList<Graphics.Texture> Textures { get; protected set; }
        public IList<Graphics.Texture> SpecularTextures { get; protected set; } 
        public IList<string> TextureNames { get; protected set; } 
        public BoundingBox BoundingBox { get; protected set; }
        public BoundingBox ModelBox { get; protected set; }
        public float[] TextureScales { get; protected set; }
        public float[] SpecularFactors { get; protected set; }

        public int[] DoodadReferences { get; protected set; }
        public int[] GroundEffectLayer { get; protected set; }

        public bool IsAlphaChanged { get; set; }
        public bool TexturesChanged { get; set; }
        public bool DoodadsChanged { get; set; }

        public Mcly[] Layers { get { return mLayers; } }

        public WeakReference<MapArea> Parent { get; protected set; }

        protected MapChunk()
        {
            HoleValues = new byte[64];
            for (var i = 0; i < 64; ++i) HoleValues[i] = 0xFF;

            GroundEffectLayer = new int[64];

            Vertices = new AdtVertex[145];
            AlphaValues = new uint[4096];
            DoodadReferences = new int[0];
        }

        ~MapChunk()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Textures != null)
            {
                Textures.Clear();
                Textures = null;
            }

            Vertices = null;
            AlphaValues = null;
            HoleValues = null;
            TextureScales = null;
            DoodadReferences = null;
            Parent = null;
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void UpdateNormals()
        {
            if (mUpdateNormals == false)
                return;

            mUpdateNormals = false;
            for (var i = 0; i < 145; ++i)
            {
                var p1 = Vertices[i].Position;
                var p2 = p1;
                var p3 = p2;
                var p4 = p3;
                var v = p1;

                p1.X -= 0.5f * Metrics.UnitSize;
                p1.Y -= 0.5f * Metrics.UnitSize;
                p2.X += 0.5f * Metrics.UnitSize;
                p2.Y -= 0.5f * Metrics.UnitSize;
                p3.X += 0.5f * Metrics.UnitSize;
                p3.Y += 0.5f * Metrics.UnitSize;
                p4.X -= 0.5f * Metrics.UnitSize;
                p4.Y += 0.5f * Metrics.UnitSize;

                var mgr = WorldFrame.Instance.MapManager;
                float h;
                if (mgr.GetLandHeight(p1.X, 64.0f * Metrics.TileSize - p1.Y, out h)) p1.Z = h;
                if (mgr.GetLandHeight(p2.X, 64.0f * Metrics.TileSize - p2.Y, out h)) p2.Z = h;
                if (mgr.GetLandHeight(p3.X, 64.0f * Metrics.TileSize - p3.Y, out h)) p3.Z = h;
                if (mgr.GetLandHeight(p4.X, 64.0f * Metrics.TileSize - p4.Y, out h)) p4.Z = h;

                var n1 = Vector3.Cross((p2 - v), (p1 - v));
                var n2 = Vector3.Cross((p3 - v), (p2 - v));
                var n3 = Vector3.Cross((p4 - v), (p3 - v));
                var n4 = Vector3.Cross((p1 - v), (p4 - v));

                var n = n1 + n2 + n3 + n4;
                n.Normalize();
                var tmp = n.Y;
                n.Y = n.X;
                n.X = tmp;
                n.Y *= -1;
                n.Z *= -1;

                n.X = ((sbyte) (n.X * 127)) / 127.0f;
                n.Y = ((sbyte) (n.Y * 127)) / 127.0f;
                n.Z = ((sbyte) (n.Z * 127)) / 127.0f;

                Vertices[i].Normal = n;
            }
        }

        private void CompressRow(Stream strm, int row, int layer)
        {
            var baseIndex = row * 64;

            // step 1: find ranges of identical values
            var ranges = new List<Tuple<int, int>>();
            var lastValue = (byte)((AlphaValues[baseIndex] >> (layer * 8)) & 0xFF);
            var curRangeStart = 0;
            for (var i = 1; i < 64; ++i)
            {
                var cur = (byte)((AlphaValues[i + baseIndex] >> (layer * 8)) & 0xFF);
                if (cur == lastValue)
                    continue;

                if (i - curRangeStart > 1)
                    ranges.Add(new Tuple<int, int>(curRangeStart, i));

                curRangeStart = i;
                lastValue = cur;
            }

            // step 2: Write the ranges appropriately
            var read = 0;
            while (read < 64)
            {
                var range = ranges.Count > 0 ? ranges[0] : null;
                if (range != null && range.Item1 == read)
                {
                    var value = (byte)((AlphaValues[read + baseIndex] >> (layer * 8)) & 0xFF);
                    var repeatCount = range.Item2 - range.Item1;
                    while (repeatCount >= 0x7F)
                    {
                        strm.WriteByte(0xFF);
                        strm.WriteByte(value);
                        repeatCount -= 0x7F;
                    }

                    if (repeatCount > 0)
                    {
                        strm.WriteByte((byte)(0x80 | repeatCount));
                        strm.WriteByte(value);
                    }

                    ranges.RemoveAt(0);

                    read = range.Item2;
                }
                else
                {
                    var nextRange = ranges.Count > 0 ? ranges[0] : null;
                    int repeatCount;
                    if (nextRange == null)
                        repeatCount = 64 - read;
                    else
                        repeatCount = nextRange.Item1 - read;

                    while (repeatCount >= 0x7F)
                    {
                        strm.WriteByte(0x7F);
                        for (var i = 0; i < 0x7F; ++i)
                            strm.WriteByte((byte)((AlphaValues[baseIndex + read++] >> (layer * 8)) & 0xFF));

                        repeatCount -= 0x7F;
                    }

                    if (repeatCount > 0)
                    {
                        strm.WriteByte((byte)repeatCount);
                        for (var i = 0; i < repeatCount; ++i)
                            strm.WriteByte((byte)((AlphaValues[baseIndex + read++] >> (layer * 8)) & 0xFF));
                    }
                }
            }
        }
        protected abstract int AddTextureLayer(string textureName);

        protected byte[] GetAlphaCompressed(int layer)
        {
            var strm = new MemoryStream();

            for (var i = 0; i < 64; ++i)
                CompressRow(strm, i, layer);

            return strm.ToArray();
        }

        protected byte[] GetAlphaUncompressed(int layer)
        {
            if (WorldFrame.Instance.MapManager.HasNewBlend)
            {
                var ret = new byte[4096];
                for (var i = 0; i < 4096; ++i)
                    ret[i] = (byte)((AlphaValues[i] >> (layer * 8)) & 0xFF);
                return ret;
            }
            else
            {
                var ret = new byte[2048];
                for (var i = 0; i < 2048; ++i)
                {
                    var a1 = (byte)((AlphaValues[i * 2] >> (layer * 8)) & 0xFF);
                    var a2 = (byte)((AlphaValues[i * 2 + 1] >> (layer * 8)) & 0xFF);

                    var v1 = (uint)((a1 / 255.0f) * 15.0f);
                    var v2 = (uint)((a2 / 255.0f) * 15.0f);
                    ret[i] = (byte)((v2 << 4) | v1);
                }

                return ret;
            }
        }

        private int FindTextureLayer(string texture)
        {
            var texNames = TextureNames.ToArray();
            for (var i = 0; i < texNames.Length; ++i)
            {
                if (string.Equals(texture, texNames[i], StringComparison.InvariantCultureIgnoreCase))
                    return i;
            }

            if (texNames.Length >= 4)
                return -1;

            return AddTextureLayer(texture);
        }
    }
}
