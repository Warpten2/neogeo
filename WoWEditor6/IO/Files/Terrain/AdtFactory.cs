﻿using System;

namespace WoWEditor6.IO.Files.Terrain
{
    class AdtFactory
    {
        public static AdtFactory Instance { get; private set; }

        static AdtFactory()
        {
            Instance = new AdtFactory();
        }

        public MapArea CreateArea(string continent, int ix, int iy)
        {
            return new WoD.MapArea(continent, ix, iy);
        }
    }
}
