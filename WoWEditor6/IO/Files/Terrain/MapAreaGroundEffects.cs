﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using WoWEditor6.IO.Files.Structures;
using WoWEditor6.Storage;

#pragma warning disable 219
#pragma warning disable 169

namespace WoWEditor6.IO.Files.Terrain
{
    class MapAreaGroundEffects
    {
        private int mIndexX, mIndexY;
        private static readonly Dictionary<int, string[]> GroundEffectCache = new Dictionary<int, string[]>(); 

        public void ProcessChunk(MapChunk chunk)
        {
            var layerDoodads = new Dictionary<int, string[]>();
            var groundEffects = new Dictionary<int, GroundEffectTextureEntry>();
            for (var i = 0; i < 4; ++i)
            {
                if (i >= chunk.Layers.Length || chunk.Layers[i].EffectId < 0)
                    continue;

                var row = DbcStorage.GroundEffectTexture[chunk.Layers[i].EffectId];
                if (row == null)
                    continue;

                layerDoodads.Add(i, GetDoodads(chunk.Layers[i].EffectId, row));
                groundEffects.Add(i, row);
            }

            var step = Metrics.ChunkSize / 24.0f;
            var hasDoodadsList = layerDoodads.Select(s => s.Value.Any(d => d != null)).ToList();
            for (var i = 0; i < 64; ++i)
            {
                var layer = chunk.GroundEffectLayer[i];
                if (layer < 0 || layer >= 4)
                    continue;

                if (hasDoodadsList[layer] == false)
                    continue;

                var texRow = groundEffects[layer];

                var ix = i / 64;
                var iy = i % 64;
            }
        }

        private string[] GetDoodads(int effect, GroundEffectTextureEntry textureRow)
        {
            if (GroundEffectCache.ContainsKey(effect))
                return GroundEffectCache[effect];

            var ret = new string[4];
            for (var i = 0; i < 4; ++i)
            {
                var doodadRef = textureRow.DoodadID[1 + i];
                if (doodadRef < 0)
                    continue;

                ret[i] = GetDoodadString(doodadRef);
            }

            GroundEffectCache.Add(effect, ret);
            return ret;
        }

        private string GetDoodadString(int id)
        {
            if (!DbcStorage.GroundEffectDoodad.HasKey(id))
                return null;

            return DbcStorage.GroundEffectDoodad[id].FileData.Value.CompletePath;
        }
    }
}

#pragma warning restore 219
#pragma warning restore 169