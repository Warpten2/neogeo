﻿using System.Collections.Generic;
using System.Linq;
using WoWEditor6.IO.Files.Structures;

namespace WoWEditor6.IO.Files.Sky.WoD
{
    class MapLightCollection
    {
        private readonly Dictionary<int, List<MapLight>> mLights = new Dictionary<int, List<MapLight>>();
        private readonly Dictionary<int, List<ZoneLight>> mZoneLights = new Dictionary<int, List<ZoneLight>>();

        public List<MapLight> GetLightsForMap(int mapid)
        {
            List<MapLight> ret;
            return mLights.TryGetValue(mapid, out ret) ? ret : new List<MapLight>();
        }

        public List<ZoneLight> GetZoneLightsForMap(int mapid)
        {
            List<ZoneLight> ret;
            return mZoneLights.TryGetValue(mapid, out ret) ? ret : new List<ZoneLight>();
        }

        public bool HasZoneLights(int mapid)
        {
            return mZoneLights.ContainsKey(mapid);
        }

        // ReSharper disable once FunctionComplexityOverflow
        public void FillLights()
        {
            var dataEntries = new Dictionary<uint, List<LightDataEntry>>();
            var lightDataEnumerator = Storage.DbcStorage.LightData.GetEnumerator();
            while (lightDataEnumerator.MoveNext())
            {
                var entry = lightDataEnumerator.Current.Value;
                List<LightDataEntry> l;
                if (dataEntries.TryGetValue(entry.skyId, out l))
                    l.Add(entry);
                else
                    dataEntries.Add(entry.skyId, new List<LightDataEntry> { entry });
            }

            var lightMap = new Dictionary<int, MapLight>();
            var lightMapEnumerator = Storage.DbcStorage.Light.GetEnumerator();
            while (lightMapEnumerator.MoveNext())
            {
                var lightElem = lightMapEnumerator.Current.Value;
                var light = new LightEntryData(ref lightElem);

                light.Position = new SharpDX.Vector3(light.Position.X / 36.0f, light.Position.Y / 36.0f,
                    light.Position.Z / 36.0f);
                light.Position.Z = 64.0f * Metrics.TileSize - light.Position.Z;
                light.InnerRadius /= 36.0f;
                light.OuterRadius /= 36.0f;

                var paramsData = Storage.DbcStorage.LightParams[light.RefParams];
                var l = new MapLight(light, ref paramsData);
                List<LightDataEntry> elems;
                if (dataEntries.TryGetValue((uint) light.RefParams, out elems))
                    l.AddAllData(elems);

                List<MapLight> lightList;
                if (mLights.TryGetValue(light.MapId, out lightList))
                    lightList.Add(l);
                else
                    mLights.Add(light.MapId, new List<MapLight> {l});

                lightMap[light.Id] = l;
            }

            var zoneLightMap = new Dictionary<int, ZoneLight>();
            var zoneLightEnumerator = Storage.DbcStorage.ZoneLight.GetEnumerator();
            while (zoneLightEnumerator.MoveNext())
            {
                var light = new ZoneLight();
                var zl = zoneLightEnumerator.Current.Value;
                light.SetDbcZoneLight(ref zl);
                var le = Storage.DbcStorage.Light[zl.RefLight];

                var dq = mLights[le.MapId.Raw];
                dq.RemoveAll(m => m.LightId == le.Id);

                var lp = lightMap[le.Id];
                light.Light = lp;
                List<ZoneLight> zlList;
                if (mZoneLights.TryGetValue(zl.MapId.Raw, out zlList))
                    zlList.Add(light);
                else
                    mZoneLights.Add(zl.MapId.Raw, new List<ZoneLight> {light});

                zoneLightMap[zl.Id] = light;
            }

            var zoneLightPointEnumerator = Storage.DbcStorage.ZoneLightPoint.GetEnumerator();
            while (zoneLightPointEnumerator.MoveNext())
            {
                var zp = zoneLightPointEnumerator.Current.Value;
                ZoneLight zl;
                if (zoneLightMap.TryGetValue(zp.RefZoneLight, out zl) == false)
                    continue;

                zl.AddPolygonPoint(ref zp);
            }

            foreach (var pair in zoneLightMap)
                pair.Value.CreatePolygon();

            //! FIXES WMO INSTANCE MAPS RENDERING. DON'T ASK. I HAVE NO CLUE.
            // InitGlobalLight();
        }

        private void InitGlobalLight()
        {
            var globalLightElem = Storage.DbcStorage.Light.First();
            var globalLight = new LightEntryData(ref globalLightElem);
            globalLight.Position = new SharpDX.Vector3(globalLight.Position.X / 36.0f, globalLight.Position.Y / 36.0f,
                    globalLight.Position.Z / 36.0f);
            globalLight.Position.Z = 64.0f * Metrics.TileSize - globalLight.Position.Z;
            globalLight.InnerRadius /= 36.0f;
            globalLight.OuterRadius /= 36.0f;

            var globalParams = Storage.DbcStorage.LightParams[globalLight.RefParams];
            var globalMapLight = new MapLight(globalLight, ref globalParams);

            var mapEnumerator = Storage.DbcStorage.Map.GetEnumerator();
            while (mapEnumerator.MoveNext())
            {
                var mapid = mapEnumerator.Current.Value.ID;
                if (mLights.ContainsKey(mapid) || mZoneLights.ContainsKey(mapid))
                    continue;

                mLights.Add(mapid, new List<MapLight> { globalMapLight });
            }
        }
    }
}
