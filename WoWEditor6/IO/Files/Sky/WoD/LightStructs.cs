﻿using SharpDX;
using WoWEditor6.IO.Files.Structures;

namespace WoWEditor6.IO.Files.Sky.WoD
{
    class LightEntryData
    {
        public LightEntryData(ref LightEntry e)
        {
            Id = e.Id;
            MapId = e.MapId.Raw;
            Position = e.Position;
            InnerRadius = e.InnerRadius;
            OuterRadius = e.OuterRadius;
            RefParams = e.RefParams;
            Water = e.Water;
            Sunset = e.Sunset;
            Other = e.Other;
            Death = e.Death;
        }

        public readonly int Id;
        public readonly int MapId;
        public Vector3 Position;
        public float InnerRadius;
        public float OuterRadius;
        public readonly int RefParams;
        public readonly int Water;
        public readonly int Sunset;
        public readonly int Other;
        public readonly int Death;
    }
}
