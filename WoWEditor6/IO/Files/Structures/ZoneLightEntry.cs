﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class ZoneLightEntry
    {
        [Index]
        public int Id;
        public int ofsName;
        public ForeignKey<MapEntry> MapId;
        public int RefLight;
    }
}
