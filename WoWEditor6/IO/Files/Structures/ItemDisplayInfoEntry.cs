﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class ItemDisplayInfoEntry
    {
        [Index]
        public int ID;
        private string ModelNameA;
        private string ModelNameB;
        public int ModelTextureAID;
        public int ModelTextureBID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public int[] GeosetGroup;
        public int Flags;
        public int SpellVisualID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public int[] HelmetGeosetVis;
        public int ArmUpperTextureID;
        public int ArmLowerTextureID;
        public int HandTextureID;
        public int TorsoUpperTextureID;
        public int TorsoLowerTextureID;
        public int LegUpperTextureID;
        public int LegLowerTextureID;
        public int FootTextureID;
        public int AccessoryTextureID;
        public int ItemVisual;
        public int ParticleColorID;

        public string ArmUpperTexture
        {
            get {
                if (ArmUpperTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == ArmUpperTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string ArmLowerTexture
        {
            get {
                if (ArmLowerTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == ArmLowerTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string HandTexture
        {
            get {
                if (HandTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == HandTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string TorsoUpperTexture
        {
            get
            {
                if (TorsoUpperTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == TorsoUpperTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string TorsoLowerTexture
        {
            get
            {
                if (TorsoLowerTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == TorsoLowerTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string LegUpperTexture
        {
            get
            {
                if (LegUpperTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == LegUpperTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string LegLowerTexture
        {
            get
            {
                if (LegLowerTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == LegLowerTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string FootTexture
        {
            get
            {
                if (FootTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == FootTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string AccessoryTexture
        {
            get
            {
                if (AccessoryTextureID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == AccessoryTextureID && t.TextureType == 3 /* Item texture */).Path;
            }
        }

        public string ModelTextureA
        {
            get
            {
                if (ModelTextureAID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == ModelTextureAID && t.TextureType == 2 /* both genders */).Path;
            }
        }

        public string ModelTextureB
        {
            get
            {
                if (ModelTextureBID == 0)
                    return null;
                return Storage.DbcStorage.TextureFileData.First(t => t.TextureItemID == ModelTextureBID && t.TextureType == 2 /* both genders */).Path;
            }
        }
    }
}
