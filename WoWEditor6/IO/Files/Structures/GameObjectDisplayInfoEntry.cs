﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    // Incomplete but sufficient
    class GameObjectDisplayInfoEntry
    {
        [Index]
        public int ID;
        public ForeignKey<FileDataEntry> FileData;
    }
#pragma warning restore 0649
}
