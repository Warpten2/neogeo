﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class LoadingScreenEntry
    {
        [Index]
        public uint ID;
        public string Name;
        public string Path;
        public uint HasWidescreen;
    }
#pragma warning restore 0649
}
