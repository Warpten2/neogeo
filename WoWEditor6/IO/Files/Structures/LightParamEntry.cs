﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class LightParamsEntry
    {
        [Index]
        public int Id;
        public int HightlightSky;
        public int LightSkyboxId;
        public int CloudTypeId;
        public float Glow;
        public float WaterAlphaShallow;
        public float WaterAlphaDeep;
        public float OceanAlphaShallow;
        public float OceanAlphaDeep;
        public int flags;
    }
}
