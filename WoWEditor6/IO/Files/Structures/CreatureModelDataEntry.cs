﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWEditor6.Storage;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class CreatureModelDataEntry
    {
        [Index]
        public int ID;
        public int Flags;
        public int FileDataID;
        public int SizeClass;
        public float ModelScale;
        public int BloodID;
        public int FootprintTextureID;
        public float FootprintTextureLength;
        public float FootprintTextureWidth;
        public float FootprintParticleScale;
        public int FoleyMaterialID;
        public int FootstepShakeSize;
        public int DeathThudShakeSize;
        public int SoundID;
        public float CollisionWidth;
        public float CollisionHeight;
        public float MountHeight;
        public Vector3 GeoBoxMin;
        public Vector3 GeoBoxMax;
        public float WorldEffectScale;
        public float AttachedEffectScale;
        public float MissileCollisionRadius;
        public float MissileCollisionPush;
        public float MissileCollisionRaise;
        public float OverrideLootEffectScale;
        public float OverrideNameScale;
        public float OverrideSelectionRadius;
        public float TamedPetBaseScale;
        public int CreatureGeosetDataId;
        public float HoverHeight;

        public string ModelPath
        {
            get
            {
                if (DbcStorage.FileData.HasKey(FileDataID))
                    return DbcStorage.FileData[FileDataID].CompletePath;
                return null;
            }
        }
    }
#pragma warning disable 0649
}
