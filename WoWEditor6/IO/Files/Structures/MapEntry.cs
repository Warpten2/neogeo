﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class MapEntry
    {
        [Index]
        public int ID;
        public string Directory;
        public int InstanceType;
        public int Flags;
        public int MapType;
        public int unk5;
        public string MapName;
        public int AreaTableID;
        public string MapDescription0;
        public string MapDescription1;
        public ForeignKey<LoadingScreenEntry> LoadingScreen;
        public float MinimapIconScale;
        public int CorpseMapID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public float[] CorpsePos;
        public int TimeOfDayOverride;
        public int ExpansionID;
        public int RaidOffset;
        public int MaxPlayers;
        public ForeignKey<MapEntry> ParentMap;
        public ForeignKey<MapEntry> CosmeticParentMap;
        public int TimeOffset;
    }
#pragma warning restore 0649
}
