﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class GroundEffectTextureEntry
    {
        [Index]
        public int ID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] DoodadID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int DoodadWeight;
        public int Density;
        public int Sound;
    };
#pragma warning restore 0649
}
