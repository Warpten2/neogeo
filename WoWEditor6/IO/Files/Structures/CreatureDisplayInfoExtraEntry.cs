﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class CreatureDisplayInfoExtraEntry
    {
        [Index]
        public int ID;
        public int RaceID;
        public int Gender;
        public int SkinID;
        public int FaceID;
        public int HairStyleID;
        public int HairColorID;
        public int FacialHair;
        public int HelmID;
        public int ShoulderID;
        public int ShirtID;
        public int CuirassID;
        public int BeltID;
        public int LegsID;
        public int BootsID;
        public int WristsID;
        public int GlovesID;
        public int TabardID;
        public int CapeID;
        public int Flags;
        public int FileDataID;
        public int HDFileDataID;
    }
#pragma warning restore 0649
}
