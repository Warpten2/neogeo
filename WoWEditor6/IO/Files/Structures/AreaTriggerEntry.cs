﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    class AreaTriggerEntry
    {
        [Index]
        public int ID;
        public ForeignKey<MapEntry> Map;
        public Vector3 Position;
        public int PhaseUseFlags;
        public int PhaseID;
        public int PhaseGroupID;
        public float Radius;
        public float BoxLength;
        public float BoxWidth;
        public float BoxHeight;
        public float BoxYaw;
        public int ShapeType;
        public int ShapeID;
        public int ActionSetID;
        public int Flags;
    }
}
