﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649, 0169
    class CreatureDisplayInfoEntry
    {
        [Index]
        public int ID;
        public ForeignKey<CreatureModelDataEntry> ModelData;
        public int SoundDataID;
        public ForeignKey<CreatureDisplayInfoExtraEntry> ExtraDisplayInfo;
        public float Scale;
        public float Unk620;
        public int Maaaybe;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.SysInt)]
        public string[] TextureVariation;
        private string PortraitTextureP;
        public int SizeClass;
        public int BloodID;
        public int NpcSoundID;
        public int ParticleColorID;
        public int CreatureGeosetData;
        public int ObjectEffectPackageID;
        public int AnimReplacementSetID;
        public int Flags;
        public int Gender;
        public int StateSpellVisualKitID;
    }
#pragma warning restore 0649, 0169
}
