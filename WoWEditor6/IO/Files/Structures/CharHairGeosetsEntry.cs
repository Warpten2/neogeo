﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class DbcCharHairGeosets
    {
        [Index()]
        public readonly int ID;
        public readonly int Race;
        public readonly int Gender;
        public readonly int VariationID;
        public readonly int Variation;
        public readonly int GeosetID;
        public readonly int GeosetType;
        public readonly int ShowScalp;
        public readonly int ColorIndex;
    }
}
