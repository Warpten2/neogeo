﻿using WoWEditor6.Storage;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649, 0169
    class TextureFileDataEntry
    {
        [Index]
        public int ID;
        public int TextureItemID;
        private int Unk;
        public int TextureType;
        public ForeignKey<FileDataEntry> FileData;

        public string Path
        {
            get
            {
                return FileData.Value.CompletePath;
            }
        }
    }
#pragma warning restore 0649, 0169
}
