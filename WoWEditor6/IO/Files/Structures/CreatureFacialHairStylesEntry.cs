﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class CharacterFacialHairStylesEntry
    {
        [Index]
        public int ID;
        public int Race;
        public int Gender;
        public int VariationID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] Geoset;
    }
}
