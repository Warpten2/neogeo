﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class LightDataEntry
    {
        [Index]
        public uint id;
        public uint skyId;
        public uint timeValues;
        public uint globalDiffuse;
        public uint globalAmbient;
        public uint skyColor0;
        public uint skyColor1;
        public uint skyColor2;
        public uint skyColor3;
        public uint skyColor4;
        public uint fogColor;
        public uint sunColor;
        public uint haloColor;
        public uint cloudColor;
        private uint unk1;
        private uint unk2;
        private uint unk3;
        public uint darkWater;
        public uint lightWater;
        public uint shadowColor;
        public uint unk4;
        public float fogEnd;
        public float fogScaler;
        public float unk5;
        public float fogDensity;
    }
}
