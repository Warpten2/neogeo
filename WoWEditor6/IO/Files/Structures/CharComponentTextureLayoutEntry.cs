﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class CharComponentTextureLayoutEntry
    {
        [Index]
        public int ID;
        public int Width;
        public int Height;
    }
#pragma warning restore 0649
}
