﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class FileDataEntry
    {
        [Index]
        public int ID;
        public string FileName;
        public string FilePath;

        public string CompletePath { get { return FilePath + FileName; } }
    }
#pragma warning restore 0649
}
