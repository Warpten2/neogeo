﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class CharComponentTextureSectionEntry
    {
        [Index]
        public int ID;
        public int LayoutID;
        public int X;
        public int Y;
        public int Width;
        public int Height;

        public CharComponentTextureLayoutEntry LayoutEntry
        {
            get
            {
                return Storage.DbcStorage.CharComponentTextureLayouts[LayoutID];
            }
        }
    }
#pragma warning restore 0649
}
