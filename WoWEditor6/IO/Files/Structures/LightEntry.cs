﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class LightEntry
    {
        [Index]
        public int Id;
        public ForeignKey<MapEntry> MapId;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.SysInt)]
        public Vector3 Position;
        public float InnerRadius;
        public float OuterRadius;
        public int RefParams;
        public int Water;
        public int Sunset;
        public int Other;
        public int Death;
    }
}
