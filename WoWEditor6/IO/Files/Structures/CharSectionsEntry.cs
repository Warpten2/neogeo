﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class CharSectionsEntry
    {
        [Index()]
        public uint ID;
        public uint RaceID;
        public uint SexID;
        public uint BaseSection;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public string[] Texture;
        public int Flags;
        public int VariationIndex;
        public int ColorIndex;
    }
#pragma warning restore 0649
}
