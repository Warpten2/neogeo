﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
    [StructLayout(LayoutKind.Sequential)]
    class ZoneLightPointEntry
    {
        [Index]
        public int Id;
        public int RefZoneLight;
        public float X, Z;
        public int Counter;
    }
}
