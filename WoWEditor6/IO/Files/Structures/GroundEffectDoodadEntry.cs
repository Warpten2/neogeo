﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWEditor6.IO.Files.Structures
{
#pragma warning disable 0649
    class GroundEffectDoodadEntry
    {
        [Index]
        public int ID;
        public ForeignKey<FileDataEntry> FileData;
        public int Flags;
        public float AnimScale;
        public float PushScale;
    }
#pragma warning restore 0649
}
