﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace WoWEditor6.IO.Files
{
    unsafe class DBFile<T> where T : class, new()
    {
        public int RecordSize { get; private set; }
        public int HeaderSize { get; private set; }
        public int Count { get; private set; }
        public bool DBC { get; private set; }
        public bool DB2 { get; private set; }
        public string FileName { get; private set; }

        public Dictionary<int, T> Records { get; private set; }

        public T this[int index]
        {
            get
            {
                if (Records.ContainsKey(index))
                    return Records[index];
                return null;
            }
        }

        public Dictionary<int, T>.Enumerator GetEnumerator() => Records.GetEnumerator();

        public bool HasKey(int index)
        {
            return Records.ContainsKey(index);
        }

        public T GetRow(int position)
        {
            return Records.Values.ElementAt(position);
        }

        public T First() => Records.Values.First();
        public T First(Func<T, bool> pred) => Records.Values.First(pred);
        public T Last(Func<T, bool> pred) => Records.Values.Last(pred);
        public IEnumerable<T> Where(Func<T, bool> pred) => Records.Values.Where(pred);
        public IEnumerable<U> Select<U>(Func<T, U> pred) => Records.Values.Select(pred);

        public DBFile(string fileName)
        {
            var stream = FileManager.Instance.Provider.OpenFile(@"DBFilesClient\" + fileName + ".dbc");
            if (stream == null)
                stream = FileManager.Instance.Provider.OpenFile(@"DBFilesClient\" + fileName + ".db2");

            FileName = fileName;

            if (stream == null)
                throw new FileNotFoundException(fileName);

            using (var reader = new BinaryReader(stream))
            {
                var signature = reader.ReadInt32();
                DBC = signature == 0x43424457;
                DB2 = signature == 0x32424457;

                Count = reader.ReadInt32();
                var fieldCount = reader.ReadInt32();
                RecordSize = reader.ReadInt32();

                // var marshalSize = Marshal.SizeOf(typeof(T));
                // if (RecordSize != marshalSize)
                //     Log.Warning($"Record Size for {fileName}: Header expects {RecordSize} bytes of data, structure exposes {marshalSize} bytes.");

                var stringTableSize = reader.ReadInt32();
                var copyTableSize = 0;
                var minId = 0;
                var maxId = 0;

                if (!DBC)
                {
                    reader.BaseStream.Position += 3 * 4; // Skip hash and build + timestamp last write
                    minId = reader.ReadInt32();
                    maxId = reader.ReadInt32();
                    reader.ReadInt32(); // Locale
                    copyTableSize = reader.ReadInt32();
                }

                HeaderSize = (int)reader.BaseStream.Position;

                if (maxId != 0)
                    reader.BaseStream.Position += (4 + 2) * (maxId - minId + 1);

                var recordOffset = reader.BaseStream.Position;
                reader.BaseStream.Position += Count * RecordSize;

                var StringTable = new Dictionary<int, string>();
                var stringTable = reader.ReadBytes(stringTableSize);
                var curPosition = 0;
                var stringLength = 0;
                while (curPosition < stringTable.Length)
                {
                    if (stringTable[curPosition] == 0x00)
                    {
                        StringTable.Add(curPosition - stringLength,
                            Encoding.UTF8.GetString(stringTable, curPosition - stringLength, stringLength));
                        stringLength = -1; // Back to 0 below
                    }

                    ++curPosition;
                    ++stringLength;
                }

                reader.BaseStream.Position = recordOffset;

                var erroredFields = new List<FieldInfo>();
                Records = new Dictionary<int, T>(Count);
                for (var i = 0; i < Count; ++i)
                    LoadRow(i, reader, ref StringTable, ref erroredFields);

                stream.Close();
                stream.Dispose();
            }
        }

        private T LoadRow(int rowIndex, BinaryReader reader, ref Dictionary<int, string> stringTable, ref List<FieldInfo> erroredFields)
        {
            var record = new T();
            var indexValue = -1;
            reader.BaseStream.Position = HeaderSize + rowIndex * RecordSize;
            foreach (var field in typeof(T).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic))
            {
                var fieldOffsetAttr = field.GetCustomAttribute<FieldOffsetAttribute>();
                if (typeof(T).IsExplicitLayout && fieldOffsetAttr != null)
                    reader.BaseStream.Position += fieldOffsetAttr.Value;

                var arraySizeAttr = field.GetCustomAttribute<MarshalAsAttribute>();
                var arraySize = field.FieldType.IsArray && arraySizeAttr != null ? arraySizeAttr.SizeConst : 1;

                var fieldType = field.FieldType.IsArray ? field.FieldType.GetElementType() : field.FieldType;
                var fieldValue = Array.CreateInstance(fieldType, arraySize);

                var isIndex = field.GetCustomAttribute<IndexAttribute>() != null;
                for (var i = 0; i < arraySize; ++i)
                {
                    switch (Type.GetTypeCode(fieldType))
                    {
                        case TypeCode.UInt64:
                            fieldValue.SetValue(reader.ReadUInt64(), i);
                            break;
                        case TypeCode.Int64:
                            fieldValue.SetValue(reader.ReadInt64(), i);
                            break;
                        case TypeCode.UInt32:
                            if (isIndex)
                            {
                                indexValue = reader.ReadInt32();
                                fieldValue.SetValue((uint)indexValue, i);
                            }
                            else
                                fieldValue.SetValue(reader.ReadUInt32(), i);
                            break;
                        case TypeCode.Int32:
                            if (isIndex)
                            {
                                indexValue = reader.ReadInt32();
                                fieldValue.SetValue(indexValue, i);
                            }
                            else
                                fieldValue.SetValue(reader.ReadInt32(), i);
                            break;
                        case TypeCode.UInt16:
                            fieldValue.SetValue(reader.ReadUInt16(), i);
                            break;
                        case TypeCode.Int16:
                            fieldValue.SetValue(reader.ReadInt16(), i);
                            break;
                        case TypeCode.Byte:
                            fieldValue.SetValue(reader.ReadByte(), i);
                            break;
                        case TypeCode.SByte:
                            fieldValue.SetValue(reader.ReadSByte(), i);
                            break;
                        case TypeCode.Single:
                            fieldValue.SetValue(reader.ReadSingle(), i);
                            break;
                        case TypeCode.String:
                            var stringIndex = reader.ReadInt32();
                            if (stringTable.ContainsKey(stringIndex))
                                fieldValue.SetValue(stringTable[stringIndex], i);
                            else if (erroredFields.Contains(field)) {
                                Log.Warning($"Field {field.Name} is likely not a string!");
                                erroredFields.Add(field);
                            }
                            break;
                        case TypeCode.Object:
                            if (!fieldType.IsGenericType) // Custom object - read with marshaller
                            {
                                var rawData = reader.ReadBytes(Marshal.SizeOf(fieldType));
                                GCHandle handle = GCHandle.Alloc(rawData, GCHandleType.Pinned);
                                fieldValue.SetValue(Marshal.PtrToStructure(handle.AddrOfPinnedObject(), fieldType), i);
                                handle.Free();
                            }
                            else // ForeignKey<T>
                            {
                                if (field.FieldType.IsArray)
                                    throw new Exception($"DBFile.cs: ForeignKey<T> cannot be (yet) used as an array type! (FileName: {FileName})");
                                dynamic foreignKey = Activator.CreateInstance(typeof(ForeignKey<>).MakeGenericType(fieldType.GetGenericArguments()));
                                foreignKey.Raw = reader.ReadInt32();
                                field.SetValue(record, foreignKey);
                            }
                            break;
                    }
                }

                // ForeignKey<T> written already a few lines above
                if (fieldType.IsGenericType)
                    continue;

                if (field.FieldType.IsArray)
                    field.SetValue(record, fieldValue);
                else
                    field.SetValue(record, fieldValue.GetValue(0));
            }

            if (indexValue == -1)
            {
                Log.Fatal($"File {FileName}'s structure does not have any field marked as index!");
                throw new Exception($"File {FileName}'s structure does not have any field marked as index!");
            }

            Records.Add(indexValue, record);
            return record;
        }
    }
}
