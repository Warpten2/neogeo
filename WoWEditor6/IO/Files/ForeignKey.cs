﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WoWEditor6.Storage;

namespace WoWEditor6.IO.Files
{
    class ForeignKey<T> where T : class, new()
    {
        public int Raw;

        public T Value
        {
            get
            {
                return DbcStorage.OfType<T>()[Raw];
            }
        }

        public static implicit operator T(ForeignKey<T> value)
        {
            return DbcStorage.OfType<T>()[value.Raw];
        }
    }
}
