﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace WoWEditor6.IO
{
    class FileManager
    {
        public static FileManager Instance { get; private set; }

        public IFileProvider Provider { get; private set; }
        public IFileListing FileListing { get; set; }
        public string DataPath { get; set; }

        public event Action LoadComplete;

        static FileManager()
        {
            Instance = new FileManager();
        }

        FileManager()
        {
            FileListing = new DefaultFileListing();
        }

        public Stream GetExportStream(string path)
        {
            
            var fullPath = Path.Combine(Properties.Settings.Default.ExportPath ?? ".\\Export", path);
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath) ?? ".");
                return File.Open(fullPath, FileMode.Create, FileAccess.Write, FileShare.None);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Stream GetOutputStream(string path)
        {
            var fullPath = Path.Combine(Properties.Settings.Default.OutputPath ?? ".\\Output", path);
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath) ?? ".");
                return File.Open(fullPath, FileMode.Create, FileAccess.Write, FileShare.None);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public Stream GetExistingFile(string path)
        {
            try
            {
                var fullPath = Path.Combine(Directory.GetCurrentDirectory(), "Output", path);
                if (!File.Exists(fullPath))
                    return null;

                using (var strm = File.OpenRead(fullPath))
                {
                    var retStream = new MemoryStream();
                    strm.CopyTo(retStream);
                    retStream.Position = 0;
                    return retStream;
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        public void InitFromPath()
        {
            if(string.IsNullOrEmpty(DataPath))
                throw new InvalidOperationException("Cannot initialize file system without a path");

            if (File.Exists(Path.Combine(DataPath, ".build.info")))
            {
                Files.Sky.SkyManager.InitVersion();

                var mgr = new CASC.FileManager();
                mgr.LoadComplete += () =>
                {
                    Storage.DbcStorage.Initialize();
                    if (LoadComplete != null)
                        LoadComplete();
                };

                Provider = mgr;
                mgr.Initialize(DataPath);
            }
        }
    }
}
