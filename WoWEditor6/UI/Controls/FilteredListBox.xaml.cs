﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace WoWEditor6.UI.Controls
{
    /// <summary>
    /// Logique d'interaction pour FilteredListBox.xaml
    /// </summary>
    public partial class FilteredListBox : UserControl
    {
        public FilteredListBox()
        {
            InitializeComponent();

            _unfilteredItems.CollectionChanged += OnUnfilteredCollectionChanged;
            ListControl.SelectionChanged += (s, e) =>
            {
                if (SelectionChanged != null)
                    SelectionChanged(s, e);
            };
        }

        public void BeginUpdate()
        {
            _unfilteredItems.CollectionChanged -= OnUnfilteredCollectionChanged;
        }

        public void EndUpdate()
        {
            UpdateVisibleElements();
            _unfilteredItems.CollectionChanged += OnUnfilteredCollectionChanged;
            ListControl.Items.Refresh();
        }

        private void OnUnfilteredCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateVisibleElements();
        }

        public event SelectionChangedEventHandler SelectionChanged;

        static FilteredListBox()
        {
            LabelContentProperty = DependencyProperty.Register("LabelContent", typeof(string), typeof(FilteredListBox), new FrameworkPropertyMetadata(string.Empty));
            ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(FilteredListBox), new FrameworkPropertyMetadata(default(TextBlock)));
        }

        public static DependencyProperty LabelContentProperty;
        public static DependencyProperty ItemTemplateProperty;

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public string LabelContent
        {
            get { return (string)GetValue(LabelContentProperty); }
            set { SetValue(LabelContentProperty, value); }
        }

        private ObservableCollection<object> _unfilteredItems = new ObservableCollection<object>();

        public ObservableCollection<object> UnfilteredItems { get { return _unfilteredItems; } }
        public object _filteredItemsLock = new object();

        public int SelectedIndex { get { return ListControl.SelectedIndex; } }
        public object SelectedItem { get { return ListControl.SelectedItem; } }
        public System.Collections.IList SelectedItems { get { return ListControl.SelectedItems; } }
        public string FilterValue {
            get {
                if (!Dispatcher.CheckAccess())
                    return Dispatcher.Invoke(() => FilterBox.Text);
                return FilterBox.Text;
            }
        }

        private Func<object, bool> _filterPredicate;
        public Func<object, bool> Filter
        {
            get { return _filterPredicate; }
            set { _filterPredicate = value; }
        }

        public Func<object, object> OrderPredicate
        {
            get; set;
        } = null;

        public IEnumerable<T> Get<T>()
        {
            return ListControl.Items.Cast<T>();
        }

        public void AddItem(object item)
        {
            _unfilteredItems.Add(item);
        }

        public void RemoveItem(object item)
        {
            _unfilteredItems.Remove(item);
        }

        public void AddRange(object[] item)
        {
            BeginUpdate();
            _unfilteredItems.AddRange(item);
            EndUpdate();
        }

        public void AddRange(IEnumerable<object> items)
        {
            BeginUpdate();
            _unfilteredItems.AddRange(items);
            EndUpdate();
        }

        public void UpdateVisibleElements()
        {
            ListControl.UnselectAll();

            if (string.IsNullOrEmpty(FilterValue))
            {
                if (OrderPredicate != null)
                    ListControl.ItemsSource = _unfilteredItems.OrderBy(OrderPredicate);
                else
                    ListControl.ItemsSource = _unfilteredItems;
            }
            else
            {
                var newCollection = new ObservableCollection<object>();
                foreach (var item in _unfilteredItems)
                    if (_filterPredicate == null || _filterPredicate(item))
                        newCollection.Add(item);

                if (OrderPredicate != null)
                    ListControl.ItemsSource = newCollection.OrderBy(OrderPredicate);
                else
                    ListControl.ItemsSource = newCollection;
            }
        }

        private void OnFilterUpdate(object sender, TextChangedEventArgs e)
        {
            UpdateVisibleElements();
        }
    }
}
