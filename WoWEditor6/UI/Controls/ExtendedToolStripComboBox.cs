﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WoWEditor6.UI.Controls
{
    public partial class ExtendedToolStripComboBox : ToolStripControlHost
    {
        public string Label { get; set; }
        public ComboBox ComboBox { get; private set; } = new ComboBox();

        public ExtendedToolStripComboBox() : base(new FlowLayoutPanel())
        {
            InitializeComponent();
            var flowControl = Control as FlowLayoutPanel;
            flowControl.Controls.Add(new Label()
            {
                Text = Label
            });
            flowControl.Controls.Add(ComboBox);
        }
    }
}
