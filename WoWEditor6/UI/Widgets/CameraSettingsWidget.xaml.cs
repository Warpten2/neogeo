﻿using SharpDX;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using WoWEditor6.Scene;

namespace WoWEditor6.UI.Widgets
{
    /// <summary>
    /// Logique d'interaction pour CameraSettingsWidget.xaml
    /// </summary>
    public partial class CameraSettingsWidget : UserControl
    {
        public CameraSettingsWidget()
        {
            InitializeComponent();

            var camera = (PerspectiveCamera)WorldFrame.Instance.ActiveCamera;
            if (camera != null)
            {
                FarClipSlider.Value = camera.FarClip;
                FarClipValueBox.Text = FarClipSlider.Value.ToString("#.##");

                CameraSpeedSlider.Value = WorldFrame.Instance.CamControl.SpeedFactor;
                CameraSpeedValueBox.Text = $"{(int)Math.Ceiling(CameraSpeedSlider.Value)}%";

                NearClipSlider.Value = camera.NearClip;
                NearClipValueBox.Text = NearClipSlider.Value.ToString("#.##");
            }
        }

        public void CheckBox_Loaded(object sender, RoutedEventArgs rea)
        {
            // Handlers inverted? not a typo
            RenderWMOCheckbox.IsChecked = WorldFrame.Instance.RenderMask.HasFlag(WorldFrame.RenderMasks.MapWMOs);
            RenderWMOCheckbox.Unchecked += (s, e) => WorldFrame.Instance.RenderMask &= WorldFrame.RenderMasks.MapWMOs;
            RenderWMOCheckbox.Checked += (s, e) => WorldFrame.Instance.RenderMask |= ~WorldFrame.RenderMasks.MapWMOs;

            RenderM2Checkbox.IsChecked = WorldFrame.Instance.RenderMask.HasFlag(WorldFrame.RenderMasks.MapM2s);
            RenderM2Checkbox.Unchecked += (s, e) => WorldFrame.Instance.RenderMask &= WorldFrame.RenderMasks.MapM2s;
            RenderM2Checkbox.Checked += (s, e) => WorldFrame.Instance.RenderMask |= ~WorldFrame.RenderMasks.MapM2s;
        }

        public void FarClip_SliderDragged(object sender, DragCompletedEventArgs e)
        {
            // Ortho camera not used yet
            var camera = (PerspectiveCamera)WorldFrame.Instance.ActiveCamera;
            camera.SetFarClip((float)FarClipSlider.Value);
            FarClipValueBox.Text = FarClipSlider.Value.ToString("#.##");
        }

        public void CameraSpeed_SliderDragged(object sender, DragCompletedEventArgs e)
        {
            WorldFrame.Instance.CamControl.SpeedFactor = (float)CameraSpeedSlider.Value;
            CameraSpeedValueBox.Text = $"{(int)Math.Ceiling(CameraSpeedSlider.Value)}%";
        }

        public void NearClip_SliderDragged(object sender, DragCompletedEventArgs e)
        {
            // Ortho camera not used yet
            var camera = (PerspectiveCamera)WorldFrame.Instance.ActiveCamera;
            camera.SetNearClip((float)NearClipSlider.Value);
            NearClipValueBox.Text = NearClipSlider.Value.ToString("#.##");
        }

        private void TeleportCamera(object sender, RoutedEventArgs e)
        {
            var camera = WorldFrame.Instance.ActiveCamera;
            var position = new Vector3(0.0f, 0.0f, 1000.0f);
            position.X = float.Parse(CameraTeleportX.Text);
            position.Y = float.Parse(CameraTeleportY.Text);
            camera.SetPosition(position);
        }
    }
}
