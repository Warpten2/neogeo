﻿using System;
using System.Windows;
using System.Windows.Controls;
using WoWEditor6.Scene;

namespace WoWEditor6.UI.Dialogs
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings
    {
        public Settings()
        {
            InitializeComponent();
            DayNightScalingSlider.Value = Properties.Settings.Default.DayNightScaling * 10.0f;
            UseDayNightCycle.IsChecked = Properties.Settings.Default.UseDayNightCycle;
            ChangeUseDayNight_Click(UseDayNightCycle, new RoutedEventArgs());
            DefaultDayTimeTextBox.Text = Properties.Settings.Default.DefaultDayTime.ToString();
        }

        private void DayNightDefaultTime_Changed(object sender, TextChangedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb == null || DefaultDayNightTimeIndicator == null)
                return;

            int time;
            if (!int.TryParse(tb.Text, out time))
                return;

            var ts = TimeSpan.FromMinutes(time / 2.0);
            DefaultDayNightTimeIndicator.Text = "= " + (ts.Hours.ToString("D2") + ":" + ts.Minutes.ToString("D2"));
            lock (Properties.Settings.Default)
                Properties.Settings.Default.DefaultDayTime = time;

            Properties.Settings.Default.Save();
        }

        private void ChangeUseDayNight_Click(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null)
                return;

            DefaultTimePanel.Visibility = (cb.IsChecked ?? false) ? Visibility.Collapsed : Visibility.Visible;
            DayNightCycleScalingPanel.Visibility = (cb.IsChecked ?? false) ? Visibility.Visible : Visibility.Collapsed;

            lock (Properties.Settings.Default)
                Properties.Settings.Default.UseDayNightCycle = cb.IsChecked ?? false;

            Properties.Settings.Default.Save();
        }

        private void DayNightScaling_Changed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var slider = sender as Slider;
            if (slider == null)
                return;

            lock (Properties.Settings.Default)
                Properties.Settings.Default.DayNightScaling = (float) slider.Value / 10.0f;

            Properties.Settings.Default.Save();
        }
    }
}
