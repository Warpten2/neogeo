﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WoWEditor6.Scene;
using WoWEditor6.Scene.Entities.Creature;
using WoWEditor6.Scene.Entities.GameObject;
using WoWEditor6.Storage;
using WoWEditor6.Utils;
using Xceed.Wpf.Toolkit;

namespace WoWEditor6.UI.Widgets
{
    public class ExpanderListViewModel
    {
        public object SelectedExpander { get; set; }
    }

    /// <summary>
    /// Logique d'interaction pour SpawnEditor.xaml
    /// </summary>
    public partial class SpawnEditor : UserControl
    {
        public bool CreatureMode { get { return CreatureExpander.IsExpanded; } }
        public bool GameObjectMode { get { return GameObjectExpander.IsExpanded; } }

        public SpawnEditor()
        {
            InitializeComponent();

            SpawnEditorGrid.DataContext = new ExpanderListViewModel();

            CreatureOrientationSlider.Minimum = 0;
            CreatureOrientationSlider.Maximum = 360;
            CreatureOrientationTextBox.Text = "0";

            WorldFrame.Instance.GameObjectManager.OnTemplatesLoaded += () =>
            {
                GameObjectFilteredListBox.AddRange(WorldFrame.Instance.GameObjectManager.Templates.Values);
                GameObjectFilteredListBox.Filter = item =>
                {
                    var template = item as GameObjectTemplate;
                    if (template == null)
                        return false;
                    return template.Name.Contains(GameObjectFilteredListBox.FilterValue);
                };
            };
            WorldFrame.Instance.CreatureManager.OnTemplatesLoaded += () =>
            {
                CreatureFilteredListBox.AddRange(WorldFrame.Instance.CreatureManager.Templates.Values);
                CreatureFilteredListBox.SelectionChanged += (s, e) =>
                {
                    OnCreatureTemplateSelected(CreatureFilteredListBox.SelectedItem);
                };
                CreatureFilteredListBox.Filter = item =>
                {
                    var template = item as CreatureTemplate;
                    if (template == null)
                        return false;
                    return template.MaleName.Contains(CreatureFilteredListBox.FilterValue);
                };
                CreatureFilteredListBox.OrderPredicate = item =>
                {
                    var template = item as CreatureTemplate;
                    if (template == null)
                        return false;
                    return template.Entry;
                };
            };

            WorldFrame.Instance.CreatureManager.OnCreatureInstanceSelected += () =>
            {
            };
        }

        private void OnCreatureTemplateSelected(object selectedItem)
        {
            var creatureTemplate = selectedItem as CreatureTemplate;
            if (creatureTemplate == null)
                return;

            WorldFrame.Instance.CreatureManager.SelectTemplate(creatureTemplate);
        }

        private static Regex FilterRegexp = new Regex("[^0-9.-]+");
        private void FilterAlpha(object sender, TextCompositionEventArgs e)
        {
            e.Handled = FilterRegexp.IsMatch(e.Text);
        }

        private void CreatureOrientationChangedSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            CreatureOrientationTextBox.Text = $"{CreatureOrientationSlider.Value}";
            SetOrientation((int)CreatureOrientationSlider.Value);
        }

        private void CreatureOrientationChangedTextBox(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(CreatureOrientationTextBox.Text))
            {
                CreatureOrientationSlider.Value = 0;
                SetOrientation(0);
            }
            else
            {
                CreatureOrientationSlider.Value = int.Parse(CreatureOrientationTextBox.Text);
                SetOrientation((int)CreatureOrientationSlider.Value);
            }
        }

        private void CreatureFaceNorthClicked(object sender, RoutedEventArgs e)
        {
            CreatureOrientationSlider.Value = 0;
            CreatureOrientationTextBox.Text = "0";
            SetOrientation(0);
        }

        private void CreatureFaceSouthClicked(object sender, RoutedEventArgs e)
        {
            CreatureOrientationSlider.Value = 180;
            CreatureOrientationTextBox.Text = "180";
            SetOrientation(180);
        }

        private void CreatureFaceEastClicked(object sender, RoutedEventArgs e)
        {
            CreatureOrientationSlider.Value = 90;
            CreatureOrientationTextBox.Text = "90";
            SetOrientation(90);
        }

        private void CreatureFaceWestClicked(object sender, RoutedEventArgs e)
        {
            CreatureOrientationSlider.Value = 270;
            CreatureOrientationTextBox.Text = "270";
            SetOrientation(270);
        }

        private void SetGameobjectMode(object sender, RoutedEventArgs e)
        {
            WorldFrame.Instance.CreatureManager.SelectTemplate(null);
            WorldFrame.Instance.CreatureManager.SelectedInstance = null;
        }

        private void SetCreatureMode(object sender, RoutedEventArgs e)
        {
            WorldFrame.Instance.GameObjectManager.SelectTemplate(null);
        }

        // Non-GUI functions
        private void SetOrientation(int degrees)
        {
            if (CreatureMode)
                WorldFrame.Instance.CreatureManager.SetInstanceOrientation(degrees);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
