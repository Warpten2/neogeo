﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WoWEditor6.UI.Windows
{
    /// <summary>
    /// Logique d'interaction pour MovementEditorWindow.xaml
    /// </summary>
    public partial class MovementEditorWindow : Window
    {
        private static List<Vector3> _pathPoints = new List<Vector3>();

        public static void AddPoint(ref Vector3 coordinates) => _pathPoints.Add(coordinates);
        public static void AddPoints(IEnumerable<Vector3> coordinates) => _pathPoints.AddRange(coordinates);
        public static void Clear() => _pathPoints.Clear();

        public MovementEditorWindow()
        {
            InitializeComponent();
        }

        private void MovementComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (MovementComboBox.SelectedIndex)
            {
                case 0: // Standing still
                    if (RandomMovementRangeComboBox.Visibility == Visibility.Visible)
                        RandomMovementRangeComboBox.Visibility = Visibility.Collapsed;
                    if (EditStaticPathButton.Visibility == Visibility.Visible)
                        EditStaticPathButton.Visibility = Visibility.Collapsed;
                    break;
                case 1: // Random spawnpoint movement
                    if (EditStaticPathButton.Visibility == Visibility.Visible)
                        EditStaticPathButton.Visibility = Visibility.Collapsed;
                    if (RandomMovementRangeComboBox.Visibility == Visibility.Collapsed)
                        RandomMovementRangeComboBox.Visibility = Visibility.Visible;
                    break;
                case 2: // Static path
                    if (RandomMovementRangeComboBox.Visibility == Visibility.Visible)
                        RandomMovementRangeComboBox.Visibility = Visibility.Collapsed;
                    if (EditStaticPathButton.Visibility == Visibility.Collapsed)
                        EditStaticPathButton.Visibility = Visibility.Visible;
                    break;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (MovementComboBox.SelectedIndex)
            {
                case 0: // Standing still
                    break;
                case 1: // Random spawn movement
                    float randomDistance;
                    if (!float.TryParse(RandomMovementRangeComboBox.Text, out randomDistance))
                        LogError("Unable to parse movement radius.");
                    break;
                case 2: // Static path
                    break;
            }
        }

        private void LogError(string message)
        {
            ErrorLabel.Content = message;
            ErrorLabel.Visibility = Visibility.Visible;

            var timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += (s, e) => {
                Dispatcher.Invoke(() => { ErrorLabel.Visibility = Visibility.Collapsed; });
                timer.Stop();
            };
            timer.Start();
        }
    }
}
