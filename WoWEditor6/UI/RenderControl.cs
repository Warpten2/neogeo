﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using WoWEditor6.Scene;

namespace WoWEditor6.UI
{
    public partial class RenderControl : UserControl
    {
        public RenderControl()
        {
            InitializeComponent();
        }

        private void RenderControl_Click(object sender, EventArgs e)
        {
            Focus();
        }
    }
}
