﻿using System;
using System.Windows.Forms;
using WoWEditor6.Scene;
using WoWEditor6.Graphics;
using SharpDX;
using System.Drawing;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using System.Drawing.Imaging;

namespace WoWEditor6.UI.Components
{
    public partial class NavMeshViewerControl : UserControl
    {
        private PerspectiveCamera mCamera;
        private CameraControl mCamControl;

        private ConstantBuffer mMatrixBuffer;
        private RenderTarget mTarget;
        private Bitmap mPaintBitmap;

        public NavMeshViewerControl()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.Opaque | ControlStyles.UserPaint | ControlStyles.Selectable, true);
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(System.Drawing.Color.Black);
            if (mPaintBitmap != null)
                e.Graphics.DrawImage(mPaintBitmap, new PointF(0, 0));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            /*if (Site != null && Site.DesignMode)
                return;

            if (WorldFrame.Instance == null || WorldFrame.Instance.GraphicsContext == null)
                return;

            mTarget = new RenderTarget(WorldFrame.Instance.GraphicsContext);
            mMatrixBuffer = new ConstantBuffer(WorldFrame.Instance.GraphicsContext);

            mCamera = new PerspectiveCamera();
            mCamera.ViewChanged += ViewChanged;
            mCamera.ProjectionChanged += ProjChanged;
            mCamera.SetClip(0.2f, 1000.0f);
            mCamera.SetParameters(new Vector3(10, 0, 0), Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);
            mCamControl = new CameraControl(this)
            {
                TurnFactor = 0.1f,
                SpeedFactor = 20.0f
            };

            MouseDown += OnClick;
            Resize += OnResize;
            renderTimer.Tick += OnRenderTimerTick;

            OnResize(this, null);

            renderTimer.Start();*/
        }

        private void ProjChanged(Camera arg1, Matrix arg2)
        {
            throw new NotImplementedException();
        }

        private void ViewChanged(Camera arg1, Matrix arg2)
        {
            throw new NotImplementedException();
        }

        void OnResize(object sender, EventArgs args)
        {
            

            mTarget.Resize(ClientSize.Width, ClientSize.Height, true);
            mCamera.SetAspect((float)ClientSize.Width / ClientSize.Height);
        }

        void OnClick(object sender, MouseEventArgs args)
        {
            Focus();
        }

        void OnRenderTimerTick(object sender, EventArgs args)
        {
            mCamControl.Update(mCamera, false);
            if (WorldFrame.Instance.Dispatcher.InvokeRequired)
                WorldFrame.Instance.Dispatcher.BeginInvoke(OnRenderMesh);
            else
                OnRenderMesh();
        }

        unsafe void OnRenderMesh()
        {
            /*if (mRenderer == null)
                return;

            mTarget.Clear();
            mTarget.Apply();

            var ctx = WorldFrame.Instance.GraphicsContext;
            var vp = ctx.Viewport;
            ctx.Context.Rasterizer.SetViewport(new Viewport(0, 0, ClientSize.Width, ClientSize.Height, 0.0f, 1.0f));

            ctx.Context.VertexShader.SetConstantBuffer(0, mMatrixBuffer.Native);
            mRenderer.RenderPortrait();

            mTarget.Remove();
            ctx.Context.Rasterizer.SetViewport(vp);

            ctx.Context.ResolveSubresource(mTarget.Texture, 0, mResolveTexture, 0, Format.B8G8R8A8_UNorm);
            ctx.Context.CopyResource(mResolveTexture, mMapTexture);

            var box = ctx.Context.MapSubresource(mMapTexture, 0, MapMode.Read, SharpDX.Direct3D11.MapFlags.None);
            var bmp = new Bitmap(ClientSize.Width, ClientSize.Height, PixelFormat.Format32bppArgb);
            var bmpd = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb);
            byte* ptrDst = (byte*)bmpd.Scan0.ToPointer();
            byte* ptrSrc = (byte*)box.DataPointer.ToPointer();

            for (var i = 0; i < bmp.Height; ++i)
            {
                UnsafeNativeMethods.CopyMemory(ptrDst + i * bmp.Width * 4, ptrSrc + i * box.RowPitch, bmp.Width * 4);
            }

            bmp.UnlockBits(bmpd);
            if (mPaintBitmap != null)
                mPaintBitmap.Dispose();

            mPaintBitmap = bmp;
            ctx.Context.UnmapSubresource(mMapTexture, 0);
            Invalidate();*/
        }
    }
}
