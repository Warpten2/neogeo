﻿using SharpDX;
using SharpNav;
using SharpNav.Geometry;
using SharpNav.IO.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WoWEditor6.Graphics;
using WoWEditor6.Scene;

namespace WoWEditor6.UI
{
    /// <summary>
    /// Logique d'interaction pour MovementMapGeneratorWindow.xaml
    /// </summary>
    public partial class MovementMapGeneratorWindow : Window
    {
        public int SelectedMapId { get; set; }
        public int TileX { get; set; }
        public int TileY { get; set; }

        private Mesh mMesh;

        public MovementMapGeneratorWindow()
        {
            InitializeComponent();

            GenerateNavMeshButton.Click += (s, e) => GenerateNavMesh();
        }

        public NavMeshGenerationSettings Settings
        {
            get
            {
                var cfg = NavMeshGenerationSettings.Default;
                cfg.AgentHeight = float.Parse(AgentHeightTextBox.Text);
                cfg.AgentRadius = float.Parse(AgentRadiusTextBox.Text);

                cfg.CellSize = (1600.0f / 3.0f) / 1800;
                cfg.CellHeight = float.Parse(CellHeightTextBox.Text);

                cfg.MinRegionSize = int.Parse(MinRegionSizeTextBox.Text);
                cfg.MergedRegionSize = int.Parse(MergedRegionSizeTextBox.Text);

                cfg.MaxClimb = float.Parse(MaxClimbTextBox.Text);

                cfg.MaxEdgeError = float.Parse(MaxEdgeErrorTextBox.Text);
                cfg.MaxEdgeLength = (int)Math.Ceiling(cfg.AgentRadius * 8 / cfg.CellSize);
                cfg.VertsPerPoly = 6;
                cfg.SampleDistance = 3;
                cfg.MaxSampleError = 1;

                return cfg;
            }
        }

        private void GenerateNavMesh()
        {
            var settings = Settings;
            var mapArea = WorldFrame.Instance.MapManager.GetAreaByIndex(TileX, TileY);
            if (mapArea == null)
                return;

            var vertices = mapArea.AreaFile.FullVertices.Select(i => i.Position).ToArray();
            var tris = TriangleEnumerable.FromVector3(vertices, 0, 0, vertices.Length / 3);

            var boundingBox = new BBox3(mapArea.AreaFile.BoundingBox.Minimum, mapArea.AreaFile.BoundingBox.Maximum);
            var heightField = new Heightfield(boundingBox, settings);
            heightField.RasterizeTriangles(vertices);
            heightField.FilterLedgeSpans(settings.VoxelAgentHeight, settings.VoxelMaxClimb);
            heightField.FilterLowHangingWalkableObstacles(settings.VoxelMaxClimb);
            heightField.FilterWalkableLowHeightSpans(settings.VoxelAgentHeight);

            var compactHeightfield = new CompactHeightfield(heightField, settings);
            compactHeightfield.Erode(settings.VoxelAgentRadius);
            compactHeightfield.BuildDistanceField();
            compactHeightfield.BuildRegions((int)(settings.AgentRadius / settings.CellSize) + 8, settings.MinRegionSize, settings.MergedRegionSize);

            var contourSet = compactHeightfield.BuildContourSet(settings);
            var polyMesh = new PolyMesh(contourSet, settings);
            var polyMeshDetail = new PolyMeshDetail(polyMesh, compactHeightfield, settings);

            var navBuilder = new NavMeshBuilder(polyMesh, polyMeshDetail, new SharpNav.Pathfinding.OffMeshConnection[0], settings);
            var tiledNavMesh = new TiledNavMesh(navBuilder);

            Console.WriteLine("Rasterized " + vertices.Length / 9 + " triangles.");
            Console.WriteLine("Generated " + contourSet.Count + " regions.");
            Console.WriteLine("PolyMesh contains " + polyMesh.VertCount + " vertices in " + polyMesh.PolyCount + " polys.");
            Console.WriteLine("PolyMeshDetail contains " + polyMeshDetail.VertCount + " vertices and " + polyMeshDetail.TrisCount + " tris in " + polyMeshDetail.MeshCount + " meshes.");

            new NavMeshJsonSerializer().Serialize($@"{WorldFrame.Instance.MapManager.Continent}_{TileX}_{TileY}.json", tiledNavMesh);
        }
    }
}
